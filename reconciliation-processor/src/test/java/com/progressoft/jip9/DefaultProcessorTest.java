package com.progressoft.jip9;

import com.progressoft.jip9.resultType.*;
import com.progressoft.jip9.transaction.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

public class DefaultProcessorTest {
    TransactionParser sourceParser;
    TransactionParser targetParser;

    @BeforeEach
    public void parserSetup(){
        sourceParser = Mockito.mock(TransactionParser.class);
        targetParser = Mockito.mock(TransactionParser.class);
    }
    @Test
    public void givenNullInputs_whenConstruct_thenExceptionThrown() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new DefaultProcessor(null, targetParser));
        Assertions.assertEquals("source parser is null", exception.getMessage());
        exception = Assertions.assertThrows(NullPointerException.class, () -> new DefaultProcessor(sourceParser, null));
        Assertions.assertEquals("target parser is null", exception.getMessage());
    }

    @Test
    public void givenNullInputs_whenProcess_thenExceptionThrown() {
        DefaultProcessor processor = new DefaultProcessor(sourceParser, targetParser);
        InputStream inputStream = Mockito.mock(InputStream.class);
        BiConsumer<String, TransactionResultType> biConsumer=(s, t)->{};
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> processor.process(null, inputStream,biConsumer));
        Assertions.assertEquals("source inputStream is null", exception.getMessage());
        exception = Assertions.assertThrows(NullPointerException.class, () -> processor.process(inputStream, null,biConsumer));
        Assertions.assertEquals("target inputStream is null", exception.getMessage());
        exception = Assertions.assertThrows(NullPointerException.class, () -> processor.process(inputStream, inputStream,null));
        Assertions.assertEquals("biConsumer is null", exception.getMessage());

    }

    @Test
    public void givenValidInputs_whenProcess_thenResultsAsExpected() {
        InputStream sourceInputStream = Mockito.mock(InputStream.class);
        InputStream targetInputStream = Mockito.mock(InputStream.class);
        DefaultProcessor processor = new DefaultProcessor(new MockSourceTransactionParser(), new MockTargetTransactionParser());
        Map<String, Set<? extends TransactionResultType>> actualSummaryMap = processor.process(sourceInputStream, targetInputStream);
        List<Transaction> expectedMatchingList = getExpectedMatchingList();
        List<Transaction> expectedMisMatchingList = getExpectedMisMatchingList();
        List<Transaction> expectedMissingList = getExpectedMissingList();
        Assertions.assertIterableEquals(expectedMatchingList, actualSummaryMap.get("matching-transactions"));
        Assertions.assertIterableEquals(expectedMisMatchingList, actualSummaryMap.get("mismatched-transactions"));
        Assertions.assertIterableEquals(expectedMissingList, actualSummaryMap.get("missing-transactions"));
    }

    private List<Transaction> getExpectedMissingList() {
        List<Transaction> MissingTransList = new ArrayList<>();
        MissingTransList.add(new MissingTransactionResult("TR-47884222204", new BigDecimal("1200"), "JOD",
                LocalDate.of(2020, 1, 31), FoundInFileOption.SOURCE));
        MissingTransList.add(new MissingTransactionResult("TR-47884222217", new BigDecimal("12000"), "JOD",
                LocalDate.of(2020, 2, 14), FoundInFileOption.TARGET));
        MissingTransList.add(new MissingTransactionResult("TR-47884222245", new BigDecimal("420"), "USD",
                LocalDate.of(2020, 1, 12), FoundInFileOption.TARGET));
        return MissingTransList;
    }

    private List<Transaction> getExpectedMisMatchingList() {
        List<Transaction> misMatchingTransList = new ArrayList<>();
        misMatchingTransList.add(new MisMatchingTransactionResult("TR-47884222202", new BigDecimal("20"), "JOD",
                LocalDate.of(2020, 1, 22), FoundInFileOption.SOURCE));
        misMatchingTransList.add(new MisMatchingTransactionResult("TR-47884222202", new BigDecimal("30"), "JOD",
                LocalDate.of(2020, 1, 22), FoundInFileOption.TARGET));
        misMatchingTransList.add(new MisMatchingTransactionResult("TR-47884222205", new BigDecimal("60"), "JOD",
                LocalDate.of(2020, 2, 2), FoundInFileOption.SOURCE));
        misMatchingTransList.add(new MisMatchingTransactionResult("TR-47884222205", new BigDecimal("60"), "JOD",
                LocalDate.of(2020, 2, 3), FoundInFileOption.TARGET));
        return misMatchingTransList;
    }

    private List<Transaction> getExpectedMatchingList() {
        List<Transaction> matchingTransList = new ArrayList<>();
        matchingTransList.add(new MatchingTransactionResult("TR-47884222201", new BigDecimal("140"), "USD",
                LocalDate.of(2020, 1, 20)));
        matchingTransList.add(new MatchingTransactionResult("TR-47884222203", new BigDecimal("5000"), "JOD",
                LocalDate.of(2020, 1, 25)));
        matchingTransList.add(new MatchingTransactionResult("TR-47884222206", new BigDecimal("500"), "USD",
                LocalDate.of(2020, 2, 10)));
        return matchingTransList;
    }

    static class MockSourceTransactionParser extends TransactionParser {
        @Override
        public void parse(InputStream inputStream, BiConsumer<String, Transaction> biConsumer) {
            parse(biConsumer);
        }

        private void parse(BiConsumer<String, Transaction> biConsumer) {
            biConsumer.accept("TR-47884222201", new Transaction("TR-47884222201", new BigDecimal("140"), "USD",
                    LocalDate.of(2020, 1, 20)));
            biConsumer.accept("TR-47884222202", new Transaction("TR-47884222202", new BigDecimal("20"), "JOD",
                    LocalDate.of(2020, 1, 22)));
            biConsumer.accept("TR-47884222203", new Transaction("TR-47884222203", new BigDecimal("5000"), "JOD",
                    LocalDate.of(2020, 1, 25)));
            biConsumer.accept("TR-47884222204", new Transaction("TR-47884222204", new BigDecimal("1200"), "JOD",
                    LocalDate.of(2020, 1, 31)));
            biConsumer.accept("TR-47884222205", new Transaction("TR-47884222205", new BigDecimal("60"), "JOD",
                    LocalDate.of(2020, 2, 2)));
            biConsumer.accept("TR-47884222206", new Transaction("TR-47884222206", new BigDecimal("500"), "USD",
                    LocalDate.of(2020, 2, 10)));
        }
    }

    static class MockTargetTransactionParser extends TransactionParser {
        @Override
        public void parse(InputStream inputStream, BiConsumer<String, Transaction> biConsumer) {
            parse(biConsumer);
        }

        private void parse(BiConsumer<String, Transaction> biConsumer) {
            biConsumer.accept("TR-47884222201", new Transaction("TR-47884222201", new BigDecimal("140"), "USD",
                    LocalDate.of(2020, 1, 20)));
            biConsumer.accept("TR-47884222205", new Transaction("TR-47884222205", new BigDecimal("60"), "JOD",
                    LocalDate.of(2020, 2, 3)));
            biConsumer.accept("TR-47884222206", new Transaction("TR-47884222206", new BigDecimal("500"), "USD",
                    LocalDate.of(2020, 2, 10)));
            biConsumer.accept("TR-47884222202", new Transaction("TR-47884222202", new BigDecimal("30"), "JOD",
                    LocalDate.of(2020, 1, 22)));
            biConsumer.accept("TR-47884222217", new Transaction("TR-47884222217", new BigDecimal("12000"), "JOD",
                    LocalDate.of(2020, 2, 14)));
            biConsumer.accept("TR-47884222203", new Transaction("TR-47884222203", new BigDecimal("5000"), "JOD",
                    LocalDate.of(2020, 1, 25)));
            biConsumer.accept("TR-47884222245", new Transaction("TR-47884222245", new BigDecimal("420"), "USD",
                    LocalDate.of(2020, 1, 12)));
        }
    }

}
