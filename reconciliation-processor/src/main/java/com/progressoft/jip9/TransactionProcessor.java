package com.progressoft.jip9;

import com.progressoft.jip9.resultType.MatchingTransactionResult;
import com.progressoft.jip9.resultType.MisMatchingTransactionResult;
import com.progressoft.jip9.resultType.MissingTransactionResult;
import com.progressoft.jip9.resultType.TransactionResultType;
import com.progressoft.jip9.transaction.Transaction;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.BiConsumer;

public interface TransactionProcessor {
    void process(InputStream sourcePath, InputStream targetPath, BiConsumer<String, TransactionResultType> consumer);

    default Map<String, Set<? extends TransactionResultType>> process(InputStream sourcePath, InputStream targetPath) {
        ResultsMap resultMap = new ResultsMap();
        process(sourcePath, targetPath, resultMap::distribute);
        return resultMap.getSummaryMap();
    }


    default void failedIfNull(Object object, String name) {
        if (object == null) {
            throw new NullPointerException(name + " is null");
        }
    }

    class ResultsMap {
        private final Set<MatchingTransactionResult> matchingTransactionList;
        private final Set<MisMatchingTransactionResult> misMatchingTransactionList;
        private final Set<MissingTransactionResult> missingTransactionList;
        private final Map<String, Set<? extends TransactionResultType>> summaryMap;

        private ResultsMap() {
            matchingTransactionList = new TreeSet<>();
            misMatchingTransactionList = new TreeSet<>();
            missingTransactionList = new TreeSet<>();
            summaryMap = new LinkedHashMap<>();
        }

        Map<String, Set<? extends TransactionResultType>> getSummaryMap() {
            if (summaryMap.isEmpty()) {
                summaryMap.put("matching-transactions", matchingTransactionList);
                summaryMap.put("mismatched-transactions", misMatchingTransactionList);
                summaryMap.put("missing-transactions", missingTransactionList);
            }
            return summaryMap;
        }

        private void distribute(String key, Transaction value) {
            if (key.equals("matching-transactions"))
                matchingTransactionList.add((MatchingTransactionResult) value);
            if (key.equals("mismatched-transactions"))
                misMatchingTransactionList.add((MisMatchingTransactionResult) value);
            if (key.equals("missing-transactions"))
                missingTransactionList.add((MissingTransactionResult) value);
        }
    }
}
