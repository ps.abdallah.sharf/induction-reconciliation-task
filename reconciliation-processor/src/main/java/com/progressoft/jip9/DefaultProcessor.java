package com.progressoft.jip9;

import com.progressoft.jip9.resultType.*;
import com.progressoft.jip9.transaction.Transaction;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class DefaultProcessor implements TransactionProcessor {

    private final TransactionParser sourceParser;
    private final TransactionParser targetParser;

    public DefaultProcessor(TransactionParser sourceParser, TransactionParser targetParser) {
        failedIfNull(sourceParser, "source parser");
        failedIfNull(targetParser, "target parser");
        this.sourceParser = sourceParser;
        this.targetParser = targetParser;
    }

    public void process(InputStream sourceInputStream, InputStream targetInputStream, BiConsumer<String, TransactionResultType> biConsumer) {
        failedIfNull(sourceInputStream, "source inputStream");
        failedIfNull(targetInputStream, "target inputStream");
        failedIfNull(biConsumer, "biConsumer");
        Map<String, Transaction> sourceMap = new HashMap<>();
        sourceParser.parse(sourceInputStream, sourceMap::put);
        BiConsumer<String, Transaction> targetConsumer = ProperTargetBiConsumerGetter.getTargetBiConsumer(biConsumer, sourceMap);
        targetParser.parse(targetInputStream, targetConsumer);
        sourceMap.forEach((s, o) -> biConsumer.accept("missing-transactions", new MissingTransactionResult(o, FoundInFileOption.SOURCE)));
    }

    private static class ProperTargetBiConsumerGetter {

        private static BiConsumer<String, Transaction> getTargetBiConsumer(BiConsumer<String, TransactionResultType> biConsumer, Map<String, Transaction> sourceMap) {
            return (transactionId, targetTransaction) -> {
                Transaction sourceTransaction = sourceMap.get(transactionId);
                if (sourceTransaction != null) {
                    if (sourceTransaction.equals(targetTransaction)) {
                        biConsumer.accept("matching-transactions", new MatchingTransactionResult(sourceTransaction));
                    } else {
                        biConsumer.accept("mismatched-transactions", new MisMatchingTransactionResult(sourceTransaction, FoundInFileOption.SOURCE));
                        biConsumer.accept("mismatched-transactions", new MisMatchingTransactionResult(targetTransaction, FoundInFileOption.TARGET));
                    }
                    sourceMap.remove(transactionId);
                    return;
                }
                biConsumer.accept("missing-transactions", new MissingTransactionResult(targetTransaction, FoundInFileOption.TARGET));
            };
        }

    }
}


