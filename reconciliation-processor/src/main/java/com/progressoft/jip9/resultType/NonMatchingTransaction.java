package com.progressoft.jip9.resultType;

import com.progressoft.jip9.transaction.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class NonMatchingTransaction extends TransactionResultType {
    private FoundInFileOption foundInFile;

    public NonMatchingTransaction(Transaction transaction, FoundInFileOption foundInFile) {
        super(transaction.getTransactionId(), transaction.getAmount()
                , transaction.getCurrencyCode(), transaction.getValueDate());
        this.foundInFile = foundInFile;
    }

    public NonMatchingTransaction() {
    }

    public NonMatchingTransaction(String transactionId, BigDecimal amount, String currencyCode, LocalDate valueDate, FoundInFileOption foundInFile) {
        super(transactionId, amount, currencyCode, valueDate);
        this.foundInFile = foundInFile;
    }

    public FoundInFileOption getFoundInFile() {
        return foundInFile;
    }

    public void setFoundInFile(FoundInFileOption foundInFile) {
        this.foundInFile = foundInFile;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        NonMatchingTransaction other = (NonMatchingTransaction) obj;
        return Objects.equals(getTransactionId(), other.getTransactionId()) &&
                this.getAmount().compareTo(other.getAmount()) == 0 &&
                Objects.equals(getCurrencyCode(), other.getCurrencyCode()) &&
                Objects.equals(getValueDate(), other.getValueDate()) &&
                Objects.equals(getFoundInFile(), other.getFoundInFile());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTransactionId(), getAmount(), getCurrencyCode(), getValueDate(), foundInFile);
    }

    @Override
    public int compareTo(Transaction other) {
        NonMatchingTransaction otherTransaction = (NonMatchingTransaction) other;
        if (this.getTransactionId().equals(otherTransaction.getTransactionId())) {
            return this.getFoundInFile().compareTo(otherTransaction.getFoundInFile());
        }
        return this.getTransactionId().compareTo(otherTransaction.getTransactionId());
    }

}
