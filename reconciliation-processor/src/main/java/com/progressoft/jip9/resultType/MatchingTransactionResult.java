package com.progressoft.jip9.resultType;

import com.progressoft.jip9.transaction.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;

public class MatchingTransactionResult extends TransactionResultType {
    public MatchingTransactionResult(String transactionId, BigDecimal amount, String currencyCode, LocalDate valueDate) {
        super(transactionId, amount, currencyCode, valueDate);
    }

    public MatchingTransactionResult(Transaction transaction) {
        super(transaction.getTransactionId(), transaction.getAmount()
                , transaction.getCurrencyCode(), transaction.getValueDate());
    }

    public MatchingTransactionResult() {
        super();
    }
}
