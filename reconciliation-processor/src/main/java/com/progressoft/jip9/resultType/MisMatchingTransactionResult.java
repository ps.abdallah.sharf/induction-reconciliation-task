package com.progressoft.jip9.resultType;

import com.progressoft.jip9.transaction.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;

public class MisMatchingTransactionResult extends NonMatchingTransaction{
    public MisMatchingTransactionResult(Transaction transaction, FoundInFileOption foundInFile) {
        super(transaction, foundInFile);
    }

    public MisMatchingTransactionResult() {
    }

    public MisMatchingTransactionResult(String transactionId, BigDecimal amount, String currencyCode, LocalDate valueDate, FoundInFileOption foundInFile) {
        super(transactionId, amount, currencyCode, valueDate, foundInFile);
    }
}
