package com.progressoft.jip9.resultType;

public enum FoundInFileOption {
    SOURCE,TARGET;
    public static FoundInFileOption getInstance(String prefix){
        switch (prefix){
            case "SOURCE":
                return SOURCE;
            case "TARGET":
                return TARGET;
            default:
                throw new IllegalArgumentException();
        }
    }

}
