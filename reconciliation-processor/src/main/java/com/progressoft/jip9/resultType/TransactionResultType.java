package com.progressoft.jip9.resultType;

import com.progressoft.jip9.transaction.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;

public class TransactionResultType extends Transaction {

    public TransactionResultType(String transactionId, BigDecimal amount, String currencyCode, LocalDate valueDate) {
        super(transactionId, amount, currencyCode, valueDate);
    }

    public TransactionResultType() {
    }
}
