package com.progressoft.jip9.resultType;

import com.progressoft.jip9.transaction.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;

public class MissingTransactionResult extends NonMatchingTransaction {
    public MissingTransactionResult(Transaction transaction, FoundInFileOption foundInFile) {
        super(transaction, foundInFile);
    }

    public MissingTransactionResult() {
    }

    public MissingTransactionResult(String transactionId, BigDecimal amount, String currencyCode, LocalDate valueDate, FoundInFileOption foundInFile) {
        super(transactionId, amount, currencyCode, valueDate, foundInFile);
    }
}
