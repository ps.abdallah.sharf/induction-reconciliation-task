package com.progressoft.jip9;

import com.progressoft.jip9.exception.IllegalFormatException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class FileParserFactoryTest {
    @Test
    public void givenNullInput_whenGetProperParser_thenExceptionThrown() {
        FileParserFactory fileParserFactory=new FileParserFactory();
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> fileParserFactory.getProperParser(null));
        Assertions.assertEquals("format you've inserted is null", exception.getMessage());
    }
    @Test
    public void givenInvalidInput_whenGetProperParser_thenExceptionThrown() {
        FileParserFactory fileParserFactory=new FileParserFactory();

        IllegalFormatException exception = Assertions.assertThrows(IllegalFormatException.class, () -> fileParserFactory.getProperParser(""));
        Assertions.assertEquals("empty format", exception.getMessage());

        exception = Assertions.assertThrows(IllegalFormatException.class, () -> fileParserFactory.getProperParser("txt"));
        Assertions.assertEquals("txt is not supported", exception.getMessage());

        exception = Assertions.assertThrows(IllegalFormatException.class, () -> fileParserFactory.getProperParser("mp3"));
        Assertions.assertEquals("mp3 is not supported", exception.getMessage());
    }

}
