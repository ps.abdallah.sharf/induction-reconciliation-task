package com.progressoft.jip9;

import com.progressoft.jip9.exception.IllegalFormatException;

public class FileParserFactory implements ParserSelector {

    public TransactionParser getProperParser(String format) {
        failedIfInvalid(format);
        String upperCaseFormat=format.toUpperCase();
        switch (upperCaseFormat){
            case "CSV":
                return new CsvParser();
            case "JSON":
                return new JsonParser();
            default:
                throw new IllegalFormatException(format+" is not supported");
        }
    }

    private static void failedIfInvalid(String format) {
        if (format == null) {
            throw new NullPointerException("format you've inserted is null");
        }
        if (format.trim().isEmpty()) {
            throw new IllegalFormatException("empty format");
        }
    }
}
