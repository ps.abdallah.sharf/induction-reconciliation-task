package com.progressoft.jip9;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TransactionReconciliationApp {
    public void run(ResultPrinter resultPrinter, ParserSelector parserSelector) throws FileNotFoundException {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter source file location:");
            File sourceFile = new File(scanner.nextLine());
            TransactionParser sourceParser = getTransactionParser(parserSelector, scanner, "source");
            System.out.println("Enter target file location:");
            File targetFile = new File(scanner.nextLine());
            TransactionParser targetParser = getTransactionParser(parserSelector, scanner, "target");
            TransactionProcessor processor = new DefaultProcessor(sourceParser, targetParser);
            ProcessResult processResult=resultPrinter.printResult(processor, new FileInputStream(sourceFile), new FileInputStream(targetFile));
            printpProcessResult(processResult);
        }
    }

    private TransactionParser getTransactionParser(ParserSelector parserSelector, Scanner scanner, String file) {
        System.out.println("Enter " + file + " file format:");
        String sourceFormat = scanner.nextLine();
        return parserSelector.getProperParser(sourceFormat);
    }

    private void printpProcessResult(ProcessResult processResult) {
        System.out.println(processResult.getResult());
        System.out.println("Result files are available in directory " + processResult.getResultLocation());
    }


}
