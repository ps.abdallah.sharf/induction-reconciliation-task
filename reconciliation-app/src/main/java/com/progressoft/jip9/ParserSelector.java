package com.progressoft.jip9;

public interface ParserSelector {
    TransactionParser getProperParser(String format);

}
