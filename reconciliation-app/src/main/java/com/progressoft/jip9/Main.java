package com.progressoft.jip9;

import java.io.*;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        String distention = System.getProperty("user.home");
        ResultPrinter resultPrinter = new CSVResultPrinter(distention);
        ParserSelector parserSelector = new FileParserFactory();
        TransactionReconciliationApp app = new TransactionReconciliationApp();
        app.run(resultPrinter, parserSelector);

    }
}
