package com.progressoft.jip9;

import com.progressoft.jip9.exception.InvalidPathException;
import com.progressoft.jip9.transaction.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.BiConsumer;

public class CsvParserTest {
    @Test
    public void givenNullInputs_whenParse_thenExceptionThrown() {
        CsvParser csvParser = new CsvParser();
        BiConsumer biConsumer = Mockito.mock(BiConsumer.class);
        InputStream inputStream = Mockito.mock(InputStream.class);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> csvParser.parse(null, biConsumer));
        Assertions.assertEquals("inputStream is null", exception.getMessage());

        exception = Assertions.assertThrows(NullPointerException.class
                , () -> csvParser.parse(inputStream, null));
        Assertions.assertEquals("biConsumer is null", exception.getMessage());

    }

    @Test
    public void givenNullPath_whenParseUsingPath_thenExceptionThrown() throws IOException {
        CsvParser csvParser = new CsvParser();
        InvalidPathException exception = Assertions.assertThrows(InvalidPathException.class
                , () -> csvParser.parse(null));
        Assertions.assertEquals("path is null", exception.getMessage());

        Path notExistPath = Paths.get(".", "not-exist" + new Random().nextInt());
        exception = Assertions.assertThrows(InvalidPathException.class
                , () -> csvParser.parse(notExistPath));
        Assertions.assertEquals("path does not exists", exception.getMessage());

        Path notFilePath = Files.createTempDirectory("DefaultValidatorTest");
        exception = Assertions.assertThrows(InvalidPathException.class,
                () -> csvParser.parse(notFilePath));
        Assertions.assertEquals("path is not a file", exception.getMessage());
    }

    @Test
    public void givenValidInput_whenParse_thenResultsAsExpect() throws FileNotFoundException {
        CsvParser csvParser = new CsvParser();
        Map<String, Transaction> actualResult = csvParser.parse(Paths.get("../sample-files/input-files/bank-transactions.csv"));
        List<Transaction> expectedResult = getExpectedResult();
        Assertions.assertIterableEquals(expectedResult, actualResult.values());
    }


    private List<Transaction> getExpectedResult() {
        List<Transaction> expectedResult = new ArrayList<>();
        expectedResult.add(new Transaction("TR-47884222201", new BigDecimal("140"), "USD",
                LocalDate.of(2020, 1, 20)));
        expectedResult.add(new Transaction("TR-47884222202", new BigDecimal("20"), "JOD",
                LocalDate.of(2020, 1, 22)));
        expectedResult.add(new Transaction("TR-47884222203", new BigDecimal("5000"), "JOD",
                LocalDate.of(2020, 1, 25)));
        expectedResult.add(new Transaction("TR-47884222204", new BigDecimal("1200"), "JOD",
                LocalDate.of(2020, 1, 31)));
        expectedResult.add(new Transaction("TR-47884222205", new BigDecimal("60"), "JOD",
                LocalDate.of(2020, 2, 2)));
        expectedResult.add(new Transaction("TR-47884222206", new BigDecimal("500"), "USD",
                LocalDate.of(2020, 2, 10)));
        return expectedResult;
    }

}
