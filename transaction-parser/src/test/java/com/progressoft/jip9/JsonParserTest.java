package com.progressoft.jip9;

import com.progressoft.jip9.exception.InvalidPathException;
import com.progressoft.jip9.transaction.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.BiConsumer;

public class JsonParserTest {

    @Test
    public void givenNullInputs_whenParse_thenExceptionThrown() throws IOException {
        JsonParser jsonParser = new JsonParser();
        BiConsumer biConsumer=Mockito.mock(BiConsumer.class);
        InputStream inputStream=Mockito.mock(InputStream.class);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> jsonParser.parse(null,biConsumer));
        Assertions.assertEquals("inputStream is null", exception.getMessage());

        exception = Assertions.assertThrows(NullPointerException.class
                , () -> jsonParser.parse(inputStream,null));
        Assertions.assertEquals("biConsumer is null", exception.getMessage());

    }
    @Test
    public void givenNullPath_whenParseUsingPath_thenExceptionThrown() throws IOException {
        JsonParser jsonParser = new JsonParser();
        InvalidPathException exception = Assertions.assertThrows(InvalidPathException.class
                , () -> jsonParser.parse(null));
        Assertions.assertEquals("path is null", exception.getMessage());

        Path notExistPath = Paths.get(".", "not-exist" + new Random().nextInt());
        exception = Assertions.assertThrows(InvalidPathException.class
                , () -> jsonParser.parse(notExistPath));
        Assertions.assertEquals("path does not exists", exception.getMessage());

        Path notFilePath = Files.createTempDirectory("DefaultValidatorTest");
        exception = Assertions.assertThrows(InvalidPathException.class,
                () -> jsonParser.parse(notFilePath));
        Assertions.assertEquals("path is not a file", exception.getMessage());
    }

    @Test
    public void givenValidInput_whenParse_thenResultsAsExpect() throws FileNotFoundException {
        JsonParser jsonParser = new JsonParser();
        Path path = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        Map<String, Transaction> actualResult = jsonParser.parse(path);
        List<Transaction> expectedResult = getExpectedResult();
        Assertions.assertIterableEquals(expectedResult, actualResult.values());
    }


    private List<Transaction> getExpectedResult() {
        List<Transaction> expectedResult = new ArrayList<>();
        expectedResult.add(new Transaction("TR-47884222201", new BigDecimal("140"), "USD",
                LocalDate.of(2020, 1, 20)));
        expectedResult.add(new Transaction("TR-47884222205", new BigDecimal("60"), "JOD",
                LocalDate.of(2020, 2, 3)));
        expectedResult.add(new Transaction("TR-47884222206", new BigDecimal("500"), "USD",
                LocalDate.of(2020, 2, 10)));
        expectedResult.add(new Transaction("TR-47884222202", new BigDecimal("30"), "JOD",
                LocalDate.of(2020, 1, 22)));
        expectedResult.add(new Transaction("TR-47884222217", new BigDecimal("12000"), "JOD",
                LocalDate.of(2020, 2, 14)));
        expectedResult.add(new Transaction("TR-47884222203", new BigDecimal("5000"), "JOD",
                LocalDate.of(2020, 1, 25)));
        expectedResult.add(new Transaction("TR-47884222245", new BigDecimal("420"), "USD",
                LocalDate.of(2020, 1, 12)));
        return expectedResult;
    }

}
