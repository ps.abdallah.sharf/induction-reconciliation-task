package com.progressoft.jip9;

import com.progressoft.jip9.exception.InvalidPathException;
import com.progressoft.jip9.transaction.Transaction;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.BiConsumer;


public abstract class TransactionParser {

    public abstract void parse(InputStream inputStream, BiConsumer<String,Transaction> biConsumer);

    protected Map<String, Transaction> parse(Path path) throws FileNotFoundException {
        validatePath(path);
        Map<String,Transaction> transactionMap = new LinkedHashMap<>();
        parse(new FileInputStream(path.toFile()),transactionMap::put);
        return transactionMap;
    }

    protected void failedIfNull(Object object, String name) {
        if (object == null)
            throw new NullPointerException(name + " is null");
    }

    protected void validatePath(Path path) {
        if (path == null)
            throw new InvalidPathException("path is null");
        if (Files.notExists(path))
            throw new InvalidPathException("path does not exists");
        if (Files.isDirectory(path))
            throw new InvalidPathException("path is not a file");
    }
}
