package com.progressoft.jip9;

import com.progressoft.jip9.exception.ParsingTransactionException;
import com.progressoft.jip9.transaction.Transaction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.function.BiConsumer;

public class JsonParser extends TransactionParser {

    @Override
    public void parse(InputStream inputStream, BiConsumer<String, Transaction> biConsumer) {
        failedIfNull(inputStream, "inputStream");
        failedIfNull(biConsumer, "biConsumer");
        JSONParser jsonParser = new JSONParser();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(reader);
            jsonArray.forEach(transaction ->
                    biConsumer.accept(getTransactionObject((JSONObject) transaction).getTransactionId()
                            , getTransactionObject((JSONObject) transaction)));
        } catch (Exception exception) {
            throw new ParsingTransactionException(exception);
        }
    }

    private Transaction getTransactionObject(JSONObject jsonObject) {
        Transaction transaction = new Transaction();
        transaction.setTransactionId((String) jsonObject.get("reference"));
        transaction.setCurrencyCode((String) jsonObject.get("currencyCode"));
        transaction.setAmount(new BigDecimal((String) jsonObject.get("amount")));
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate date = LocalDate.parse((String) jsonObject.get("date"), dateTimeFormatter);
        transaction.setValueDate(date);
        return transaction;
    }

}
