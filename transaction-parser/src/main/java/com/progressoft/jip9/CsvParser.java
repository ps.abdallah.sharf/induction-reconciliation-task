package com.progressoft.jip9;

import com.progressoft.jip9.exception.ParsingTransactionException;
import com.progressoft.jip9.transaction.Transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public class CsvParser extends TransactionParser {


    @Override
    public void parse(InputStream inputStream, BiConsumer<String, Transaction> biConsumer) {
        failedIfNull(inputStream, "inputStream");
        failedIfNull(biConsumer, "biConsumer");
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            HeaderDetails headerDetails = getHeaderDetails(bufferedReader.readLine());
            readRecords(bufferedReader, biConsumer, headerDetails);
        } catch (IOException | RuntimeException exception) {
            throw new ParsingTransactionException(exception);
        }
    }

    private void readRecords(BufferedReader bufferedReader, BiConsumer<String, Transaction> biConsumer, HeaderDetails headerDetails) throws IOException {
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] lineArray = line.split(",");
            String transactionId = lineArray[headerDetails.getTransIdIndex()];
            Transaction transaction = new Transaction();
            transaction.setTransactionId(transactionId);
            transaction.setCurrencyCode(lineArray[headerDetails.getCurrencyIndex()]);
            transaction.setAmount(new BigDecimal(lineArray[headerDetails.getAmountIndex()]));
            transaction.setValueDate(LocalDate.parse(lineArray[headerDetails.getValueDateIndex()]));
            biConsumer.accept(transactionId, transaction);
        }
    }

    private HeaderDetails getHeaderDetails(String headerLine) {
        HeaderDetails headerDetails = new HeaderDetails();
        if (headerLine == null) {
            throw new IllegalArgumentException("The first is empty");
        }
        List<String> headerList = Arrays.asList(headerLine.split(","));
        headerDetails.setTransIdIndex(headerList.indexOf("trans unique id"))
                .setAmountIndex(headerList.indexOf("amount"))
                .setCurrencyIndex(headerList.indexOf("currecny"))
                .setValueDateIndex(headerList.indexOf("value date"));
        return headerDetails;
    }

    private static class HeaderDetails {

        private int transIdIndex;
        private int amountIndex;
        private int currencyIndex;
        private int valueDateIndex;

        public HeaderDetails() {
        }

        public int getTransIdIndex() {
            return transIdIndex;
        }

        public HeaderDetails setTransIdIndex(int transIdIndex) {
            this.transIdIndex = transIdIndex;
            return this;
        }

        public int getAmountIndex() {
            return amountIndex;
        }

        public HeaderDetails setAmountIndex(int amountIndex) {
            this.amountIndex = amountIndex;
            return this;
        }

        public int getCurrencyIndex() {
            return currencyIndex;
        }

        public HeaderDetails setCurrencyIndex(int currencyIndex) {
            this.currencyIndex = currencyIndex;
            return this;
        }

        public int getValueDateIndex() {
            return valueDateIndex;
        }

        public void setValueDateIndex(int valueDateIndex) {
            this.valueDateIndex = valueDateIndex;
        }

    }
}
