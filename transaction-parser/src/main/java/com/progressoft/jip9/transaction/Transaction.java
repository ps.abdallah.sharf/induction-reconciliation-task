package com.progressoft.jip9.transaction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class Transaction implements Comparable<Transaction>{
    private String transactionId;
    private BigDecimal amount;
    private String currencyCode;
    private LocalDate valueDate;

    public Transaction() {
    }

    public Transaction(String transactionId, BigDecimal amount, String currencyCode, LocalDate valueDate) {
        this.transactionId = transactionId;
        this.amount = amount;
        this.currencyCode = currencyCode;
        this.valueDate = valueDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public LocalDate getValueDate() {
        return valueDate;
    }

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        Transaction other = (Transaction) obj;
        return Objects.equals(transactionId, other.transactionId) &&
                this.amount.compareTo(other.getAmount()) == 0 &&
                Objects.equals(currencyCode, other.currencyCode) &&
                Objects.equals(valueDate, other.valueDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, amount, currencyCode, valueDate);
    }

    @Override
    public int compareTo(Transaction other) {
        return this.transactionId.compareTo(other.transactionId);
    }
}
