package com.progressoft.jip9.exception;
public class ParsingTransactionException extends RuntimeException {
    public ParsingTransactionException(java.lang.Exception cause) {
        super(cause);
    }

    public ParsingTransactionException(String message) {
        super(message);
    }

    public ParsingTransactionException() {
        super();
    }
}
