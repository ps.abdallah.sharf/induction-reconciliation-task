package com.progressoft.jip9.exception;

public class IllegalFormatException extends ParsingTransactionException {
    public IllegalFormatException(String message) {
        super(message);
    }

    public IllegalFormatException() {
        super();
    }
}
