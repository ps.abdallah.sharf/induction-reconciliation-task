package com.progressoft.jip9.exception;

public class InvalidPathException extends ParsingTransactionException {
    public InvalidPathException(String message){
        super(message);
    }

}
