package com.progressoft.jip9.business;

import com.progressoft.jip9.model.User;
import com.progressoft.jip9.service.cryptor.Cryptor;
import com.progressoft.jip9.utils.DatabaseInitializer;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserBusinessTest {

    @Test
    public void givenNullCryptor_whenConstruct_thenFail() {
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () ->
                new UserBusiness(null));
        Assertions.assertEquals("cryptor is null", thrown.getMessage());

    }
        @Test
        public void givenNullInput_whenValidateUser_thenFail() {
            Cryptor cryptor=mock(Cryptor.class);
            UserBusiness userBusiness = new UserBusiness(cryptor);

            User user = new User();
            NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () ->
                    userBusiness.isValidUser(null,user));
            Assertions.assertEquals("dataSource is null", thrown.getMessage());

            DataSource dataSource = mock(DataSource.class);
            thrown = Assertions.assertThrows(NullPointerException.class, () ->
                    userBusiness.isValidUser(dataSource,null));
            Assertions.assertEquals("user is null", thrown.getMessage());
        }

        @Test
        public void givenValidInputs_whenDoIsValidUser_thenResultAsExpected() throws SQLException, IOException {
            JdbcDataSource dataSource = new JdbcDataSource();
            Path rec_dbDb = Files.createTempFile("rec_db", ".db");
            dataSource.setUrl("jdbc:h2:file:" + rec_dbDb);
            dataSource.setUser("sa");
            dataSource.setPassword("sa");

            DatabaseInitializer initializer = new DatabaseInitializer();
            initializer.initialize(dataSource);
            try (Connection connection = dataSource.getConnection()) {
                insertDataIntoDataBase(connection);
            }
            Cryptor cryptor=getMockCryptor();
            UserBusiness userBusiness = new UserBusiness(cryptor);

            User validUser1=new User("user","password");
            Assertions.assertTrue(userBusiness.isValidUser(dataSource, validUser1));

            User validUser2=new User("root","password");
            Assertions.assertTrue(userBusiness.isValidUser(dataSource, validUser2));

            User invalidUser1=new User("user","wrongPW");
            Assertions.assertFalse(userBusiness.isValidUser(dataSource, invalidUser1));

            User invalidUser2=new User("user2","password");
            Assertions.assertFalse(userBusiness.isValidUser(dataSource, invalidUser2));

        }

    private Cryptor getMockCryptor() {
        Cryptor cryptor=mock(Cryptor.class);
        when(cryptor.checkHash("password","hashedPW")).thenReturn(true);
        when(cryptor.checkHash("password","rootHashPW")).thenReturn(true);
        return cryptor;
    }


    private void insertDataIntoDataBase(Connection connection) throws SQLException {
        String sql = "insert into rec_user values ('user','hashedPW')";
        String sql2 = "insert into rec_user values ('root','rootHashPW')";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql);
             PreparedStatement preparedStatement2 = connection.prepareStatement(sql2)) {
                preparedStatement.execute();
                preparedStatement2.execute();
            }
        }

}
