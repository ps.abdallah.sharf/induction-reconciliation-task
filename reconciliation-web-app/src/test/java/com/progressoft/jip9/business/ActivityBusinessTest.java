package com.progressoft.jip9.business;

import com.progressoft.jip9.business.absoluteBusinesses.ActivityBusiness;
import com.progressoft.jip9.model.Activity;
import com.progressoft.jip9.model.Session;
import com.progressoft.jip9.model.User;
import com.progressoft.jip9.utils.DatabaseInitializer;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

import static org.mockito.Mockito.mock;

public class ActivityBusinessTest {
    private static List<Activity> activityList;

    @BeforeAll
    public static void initializeActivitiesList() {
        activityList = new ArrayList<>();
        Activity activity1 = new Activity();
        activity1.setUserId("user");
        activity1.setSessionId("session1");
        activity1.setReconciliationTime(LocalDateTime.now());
        activity1.setSourceFileName("sourceFile1");
        activity1.setTargetFileName("targetFile1");
        activity1.setMatched(1);
        activity1.setMismatched(2);
        activity1.setMissing(3);
        activityList.add(activity1);
        Activity activity2 = new Activity();
        activity2.setUserId("user");
        activity2.setSessionId("session1");
        activity2.setReconciliationTime(LocalDateTime.now());
        activity2.setSourceFileName("sourceFile2");
        activity2.setTargetFileName("targetFile2");
        activity2.setMatched(2);
        activity2.setMismatched(2);
        activity2.setMissing(4);
        activityList.add(activity2);
    }

    @Test
    public void givenNullInput_whenAddActivity_thenFail() {
        ActivityBusiness sessionBusiness = new ActivityBusiness();
        Activity activity = new Activity();
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () ->
                sessionBusiness.addActivity(null, activity));
        Assertions.assertEquals("dataSource is null", thrown.getMessage());

        DataSource dataSource = mock(DataSource.class);
        thrown = Assertions.assertThrows(NullPointerException.class, () ->
                sessionBusiness.addActivity(dataSource, null));
        Assertions.assertEquals("activity is null", thrown.getMessage());
    }

    @Test
    public void givenNullInput_whenGetActivityMap_thenFail() {
        ActivityBusiness sessionBusiness = new ActivityBusiness();
        User user = new User();
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () ->
                sessionBusiness.getActivitiesMap(null, user));
        Assertions.assertEquals("dataSource is null", thrown.getMessage());

        DataSource dataSource = mock(DataSource.class);
        thrown = Assertions.assertThrows(NullPointerException.class, () ->
                sessionBusiness.getActivitiesMap(dataSource, null));
        Assertions.assertEquals("user is null", thrown.getMessage());
    }

    @Test
    public void givenValidInputs_whenAddActivity_thenWorkAsExpected() throws SQLException, IOException {
        JdbcDataSource dataSource = new JdbcDataSource();
        Path rec_dbDb = Files.createTempFile("rec_db", ".db");
        dataSource.setUrl("jdbc:h2:file:" + rec_dbDb);
        dataSource.setUser("sa");
        dataSource.setPassword("sa");

        User user=new User("user","hashedPW");

        Session session=new Session();
        session.setUserId("user");
        session.setUserId("session1");
        session.setUserId("user");
        session.setSessionId("session1");
        session.setSessionTime(LocalDateTime.now());
        session.setSessionTime(LocalDateTime.now());
        DatabaseInitializer initializer = new DatabaseInitializer();
        initializer.initialize(dataSource);
        List<Activity> actualSessionList;
        try (Connection connection = dataSource.getConnection()) {
            insertUserIntoDataBase(connection,user);
            insertSessionIntoDataBase(connection, session);
            ActivityBusiness activityBusiness = new ActivityBusiness();
            whenInsertValidActivities_thenDoneSuccessfuly(activityBusiness, dataSource);
            actualSessionList = retrieveDataFromDB(connection);
        }
        List<Activity> expectedSessionList = activityList;
        Assertions.assertIterableEquals(expectedSessionList, actualSessionList);

    }

    @Test
    public void givenValidInputs_whenGetActivityMap_thenResultAsExpected() throws SQLException, IOException {
        JdbcDataSource dataSource = new JdbcDataSource();
        Path rec_dbDb = Files.createTempFile("rec_db", ".db");
        dataSource.setUrl("jdbc:h2:file:" + rec_dbDb);
        dataSource.setUser("sa");
        dataSource.setPassword("sa");

        DatabaseInitializer initializer = new DatabaseInitializer();
        initializer.initialize(dataSource);
        User user=new User("user","hashedPW");
        Session session=new Session();
        session.setUserId("user");
        session.setSessionId("session1");
        session.setSessionTime(LocalDateTime.now());

        Map<LocalDateTime,List<Activity>> actualActivitiesMap;
        try (Connection connection = dataSource.getConnection()) {
            insertUserIntoDataBase(connection,user);
            insertSessionIntoDataBase(connection,session);
            insertActivitiesIntoDataBaseManually(connection);
            ActivityBusiness activityBusiness = new ActivityBusiness();
            actualActivitiesMap=activityBusiness.getActivitiesMap(dataSource,user);
        }
        Map<LocalDateTime,List<Activity>> expectedActivityMap = new TreeMap<>();
        expectedActivityMap.put(session.getSessionTime(),activityList);
        Assertions.assertIterableEquals(expectedActivityMap.keySet(), actualActivitiesMap.keySet());
        assertMapsValuesIsEqual(actualActivitiesMap.get(session.getSessionTime()));

    }

    private void assertMapsValuesIsEqual(List<Activity> actualActivities) {
        for (int i = 0; i < actualActivities.size(); i++) {
            Assertions.assertSame(actualActivities.get(i).getSessionId(),activityList.get(i).getSessionId());
            Assertions.assertEquals(actualActivities.get(i).getReconciliationTime(),activityList.get(i).getReconciliationTime());
            Assertions.assertSame(actualActivities.get(i).getMatched(),activityList.get(i).getMatched());
            Assertions.assertSame(actualActivities.get(i).getMismatched(),activityList.get(i).getMismatched());
            Assertions.assertSame(actualActivities.get(i).getSourceFileName(),activityList.get(i).getSourceFileName());
            Assertions.assertSame(actualActivities.get(i).getTargetFileName(),activityList.get(i).getTargetFileName());
        }
    }


    private void insertUserIntoDataBase(Connection connection,User user) throws SQLException {
        String sql = "insert into rec_user values (?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.execute();
        }
    }

    private void insertSessionIntoDataBase(Connection connection,Session session) throws SQLException {
        String sql = "insert into rec_session values (?,?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, session.getSessionId());
            preparedStatement.setString(2, session.getUserId());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(session.getSessionTime()));
            preparedStatement.execute();
        }
    }

    private void insertActivitiesIntoDataBaseManually(Connection connection) throws SQLException {
        String sqlQuery = "insert into rec_activity values (default ,?,?,?,?,?,?,?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {
            for (Activity activity : activityList) {
            preparedStatement.setString(1, activity.getUserId());
            preparedStatement.setString(2, activity.getSessionId());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(activity.getReconciliationTime()));
            preparedStatement.setString(4, activity.getSourceFileName());
            preparedStatement.setString(5, activity.getTargetFileName());
            preparedStatement.setInt(6, activity.getMatched());
            preparedStatement.setInt(7, activity.getMismatched());
            preparedStatement.setInt(8, activity.getMissing());
            preparedStatement.addBatch();
        }
            preparedStatement.executeBatch();
        }
    }

    private void whenInsertValidActivities_thenDoneSuccessfuly(ActivityBusiness activityBusiness, DataSource dataSource) {
        for (Activity activity : activityList) {
            Assertions.assertDoesNotThrow(() -> activityBusiness.addActivity(dataSource, activity));
        }
    }

    private List<Activity> retrieveDataFromDB(Connection connection) throws SQLException {
        List<Activity> activityList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement("select * from rec_activity")) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Activity activity = new Activity();
                    activity.setUserId(resultSet.getString("user_id"));
                    activity.setSessionId(resultSet.getString("session_id"));
                    activity.setReconciliationTime(resultSet.getTimestamp("rec_time").toLocalDateTime());
                    activity.setSourceFileName(resultSet.getString("src_file"));
                    activity.setTargetFileName(resultSet.getString("tar_file"));
                    activity.setMatched(resultSet.getInt("matched"));
                    activity.setMismatched(resultSet.getInt("mismatched"));
                    activity.setMissing(resultSet.getInt("missing"));
                    activityList.add(activity);
                }
            }
        }
        return activityList;
    }

}
