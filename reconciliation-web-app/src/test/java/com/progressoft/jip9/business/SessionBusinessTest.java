package com.progressoft.jip9.business;

import com.progressoft.jip9.model.Session;
import com.progressoft.jip9.utils.DatabaseInitializer;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

public class SessionBusinessTest {

    @Test
    public void givenNullInput_whenAddSession_thenFail() {
        SessionBusiness sessionBusiness = new SessionBusiness();
        Session session = new Session();
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () ->
                sessionBusiness.addSession(null, session));
        Assertions.assertEquals("dataSource is null", thrown.getMessage());

        DataSource dataSource = mock(DataSource.class);
        thrown = Assertions.assertThrows(NullPointerException.class, () ->
                sessionBusiness.addSession(dataSource, null));
        Assertions.assertEquals("session is null", thrown.getMessage());
    }

    @Test
    public void givenValidInputs_whenAddSession_thenWorkAsExpected() throws SQLException, IOException {
        JdbcDataSource dataSource = new JdbcDataSource();
        Path rec_dbDb = Files.createTempFile("rec_db", ".db");
        dataSource.setUrl("jdbc:h2:file:" + rec_dbDb);
        dataSource.setUser("sa");
        dataSource.setPassword("sa");

        DatabaseInitializer initializer = new DatabaseInitializer();
        initializer.initialize(dataSource);
        insertUserIntoDataBase(dataSource.getConnection());
        SessionBusiness sessionBusiness = new SessionBusiness();
        Session session1 = new Session();
        session1.setUserId("user");
        session1.setSessionId("session1");
        session1.setSessionTime(LocalDateTime.now());
        Session session2 = new Session();
        session2.setUserId("user");
        session2.setSessionId("session2");
        session2.setSessionTime(LocalDateTime.now());
        Assertions.assertDoesNotThrow(() -> sessionBusiness.addSession(dataSource, session1));
        Assertions.assertDoesNotThrow(() -> sessionBusiness.addSession(dataSource, session2));
        List<Session> expectedSessionList = retrieveDataFromDB(dataSource.getConnection());
        Assertions.assertEquals(expectedSessionList.get(0), session1);
        Assertions.assertEquals(expectedSessionList.get(1), session2);

    }
    private void insertUserIntoDataBase(Connection connection) throws SQLException {
        String sql = "insert into rec_user values ('user','hashedPW')";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.execute();
        }
    }

    private List<Session> retrieveDataFromDB(Connection connection) throws SQLException {
        List<Session> sessionList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "select * from rec_session")) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Session session = new Session();
                    session.setUserId(resultSet.getString("user_id"));
                    session.setSessionId(resultSet.getString("session_id"));
                    session.setSessionTime(resultSet.getTimestamp("session_time").toLocalDateTime());
                    sessionList.add(session);
                }
            }
        }
        return sessionList;
    }

}
