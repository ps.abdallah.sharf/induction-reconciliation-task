package com.progressoft.jip9.service.cryptor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BCryptHasherTest {
    @Test
    public void givenValidInputs_whenHashAndCheck_thenResultsAsExpected(){
        BCryptHasher cryptor=new BCryptHasher();

        String hash1=cryptor.hash("firstWord");
        String hash2=cryptor.hash("SecondWord");

        Assertions.assertTrue(cryptor.checkHash("firstWord",hash1));
        Assertions.assertFalse(cryptor.checkHash("WrongWord",hash1));

        Assertions.assertTrue(cryptor.checkHash("SecondWord",hash2));
        Assertions.assertFalse(cryptor.checkHash("WrongWord",hash2));


    }
}
