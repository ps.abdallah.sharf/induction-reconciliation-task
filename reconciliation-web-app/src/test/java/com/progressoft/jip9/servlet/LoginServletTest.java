package com.progressoft.jip9.servlet;

import com.progressoft.jip9.business.absoluteBusinesses.ActivityBusiness;
import com.progressoft.jip9.business.BusinessesWrapper;
import com.progressoft.jip9.business.SessionBusiness;
import com.progressoft.jip9.business.UserBusiness;
import com.progressoft.jip9.model.Activity;
import com.progressoft.jip9.model.Session;
import com.progressoft.jip9.model.User;
import com.progressoft.jip9.service.cryptor.Cryptor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

public class LoginServletTest {

    @Test
    public void givenInvalidUserParameters_whenCallDoPost_ThenReturnIndexPage() throws ServletException, IOException {
        String path = "/WEB-INF/views/index.jsp";
        User user = new User("user", "user");
        DataSource dataSource = Mockito.mock(DataSource.class);
        UserBusiness userBusiness = getMockUserBusiness(false);
        SessionBusiness sessionBusiness = getMockSessionBusiness();
        ActivityBusiness activityBusiness = getMockActivityBusiness();
        BusinessesWrapper businessesWrapper = BusinessesWrapper.build()
                .setDataSource(dataSource).setUserBusiness(userBusiness).setSessionBusiness(sessionBusiness).setActivityBusiness(activityBusiness);
        final LoginServlet servlet = new LoginServlet(businessesWrapper);
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getParameter("username")).thenReturn(user.getUserName());
        Mockito.when(request.getParameter("password")).thenReturn(user.getPassword());
        final HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = Mockito.mock(RequestDispatcher.class);
        when(request.getRequestDispatcher(path)).thenReturn(dispatcher);
        servlet.doPost(request, response);
        verify(request, times(1)).getRequestDispatcher(path);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void givenValidUserParameters_whenCallDoPost_ThenReturnComparePage() throws IOException {
        User user = new User("user", "user");
        DataSource dataSource = Mockito.mock(DataSource.class);
        UserBusiness userBusiness = getMockUserBusiness(true);
        SessionBusiness sessionBusiness = getMockSessionBusiness();
        ActivityBusiness activityBusiness = getMockActivityBusiness();
        BusinessesWrapper businessesWrapper = BusinessesWrapper.build()
                .setDataSource(dataSource).setUserBusiness(userBusiness).setSessionBusiness(sessionBusiness).setActivityBusiness(activityBusiness);
        final LoginServlet servlet = new LoginServlet(businessesWrapper);
        HttpSession session = mock(HttpSession.class);
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);
        Mockito.when(request.getParameter("username")).thenReturn(user.getUserName());
        Mockito.when(request.getParameter("password")).thenReturn(user.getPassword());
        final HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        when(request.getContextPath()).thenReturn("reconciliation");
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        Assertions.assertDoesNotThrow(() -> servlet.doPost(request, response));
        verify(response).sendRedirect(captor.capture());
        Assertions.assertEquals("reconciliation/compare", captor.getValue());

    }

    private ActivityBusiness getMockActivityBusiness() {
        return new ActivityBusiness() {
            @Override
            public Map<LocalDateTime, List<Activity>> getActivitiesMap(DataSource dataSource, User user) {
                return null;
            }
        };
    }

    private UserBusiness getMockUserBusiness(boolean result) {
        Cryptor cryptor=mock(Cryptor.class);
        return new UserBusiness(cryptor) {
            @Override
            public boolean isValidUser(DataSource dataSource, User user) {
                return result;
            }
        };
    }

    private SessionBusiness getMockSessionBusiness() {
        return new SessionBusiness() {
            @Override
            public void addSession(DataSource dataSource, Session session) {
            }
        };
    }
}
