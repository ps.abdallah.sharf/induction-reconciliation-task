package com.progressoft.jip9.filter;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;


public class LoginCheckFilterTest {

    private final static String path = "/WEB-INF/views/index.jsp";

    @Test
    public void givenNullUserAttribute_whenCallDoFilter_ThenReturnIndexPage() throws ServletException, IOException {

        final LoginCheckFilter servlet = new LoginCheckFilter();
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        final HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = Mockito.mock(RequestDispatcher.class);
        final FilterChain filterChain = Mockito.mock(FilterChain.class);
        when(request.getRequestDispatcher(path)).thenReturn(dispatcher);
        when(request.getServletPath()).thenReturn("/compare");
        servlet.doFilter(request, response, filterChain);
        verify(request, times(1)).getRequestDispatcher(path);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }
}
