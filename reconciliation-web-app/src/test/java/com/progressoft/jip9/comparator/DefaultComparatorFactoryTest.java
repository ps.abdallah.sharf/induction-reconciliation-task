package com.progressoft.jip9.comparator;

import com.progressoft.jip9.ParserSelector;
import com.progressoft.jip9.TransactionParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class DefaultComparatorFactoryTest {
    @Test
    public void givenNullInput_whenConstruct_thenFail() {

        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () ->
                new DefaultComparatorFactory(null));
        Assertions.assertEquals("parserSelector is null", thrown.getMessage());

    }
    @Test
    public void givenInvalidInput_whenGetProperComparator_thenFail() {
        ParserSelector parserSelector= Mockito.mock(ParserSelector.class);
        TransactionParser csvTransactionParser=Mockito.mock(TransactionParser.class);
        TransactionParser jsonTransactionParser=Mockito.mock(TransactionParser.class);
        Mockito.when(parserSelector.getProperParser("csv")).thenReturn(csvTransactionParser);
        Mockito.when(parserSelector.getProperParser("json")).thenReturn(jsonTransactionParser);
        DefaultComparatorFactory comparatorFactory=new DefaultComparatorFactory(parserSelector);
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () ->
                comparatorFactory.getComparatorInstance(null,"csv"));
        Assertions.assertEquals("source format is null", thrown.getMessage());
        thrown = Assertions.assertThrows(NullPointerException.class, () ->
                comparatorFactory.getComparatorInstance("csv",null));
        Assertions.assertEquals("target format is null", thrown.getMessage());
        IllegalArgumentException illegalArgumentException= Assertions.assertThrows(IllegalArgumentException.class, () ->
                comparatorFactory.getComparatorInstance("csv", ""));
        Assertions.assertEquals("target format is Empty", illegalArgumentException.getMessage());

    }


}
