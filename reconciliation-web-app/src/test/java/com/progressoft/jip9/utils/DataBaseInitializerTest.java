package com.progressoft.jip9.utils;

import com.progressoft.jip9.utils.DatabaseInitializer;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DataBaseInitializerTest {

    @Test
    public void givenNullDataSource_whenInitialize_thenFail() {
        DatabaseInitializer initializer = new DatabaseInitializer();
        DataSource dataSource = null;
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class,
                () -> initializer.initialize(dataSource));
        Assertions.assertEquals("null datasource", thrown.getMessage());
    }

    @Test
    public void givenValidDatasource_whenInitialize_thenSuccess() throws IOException, SQLException {
        DatabaseInitializer initializer = new DatabaseInitializer();
        JdbcDataSource dataSource = new JdbcDataSource();
        Path rec_db = Files.createTempFile("rec_db", ".db");
        dataSource.setUrl("jdbc:h2:file:" + rec_db);
        dataSource.setUser("sa");
        dataSource.setPassword("sa");

        initializer.initialize(dataSource);

        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from rec_user")) {
                try(ResultSet resultSet = preparedStatement.executeQuery()){

                }
            }
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from rec_session")) {
                try(ResultSet resultSet = preparedStatement.executeQuery()){

                }
            }
            try (PreparedStatement preparedStatement = connection.prepareStatement("select * from rec_activity")) {
                try(ResultSet resultSet = preparedStatement.executeQuery()){

                }
            }

        }
    }
}
