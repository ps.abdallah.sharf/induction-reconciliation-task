<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <TITLE>login</TITLE>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="icon" href="${pageContext.request.contextPath}/resources/favicon.jpg">


</head>
<body>

<div class="container-sm">
    <div class="w-75 p-3 mx-auto" style="margin-top: 3em">
    <form action="<%=request.getContextPath()%>/login" method="post">
        <div class="form-group">
            <td><label>username </label></td>
            <input name="username" type="text" class="form-control" required>
        </div>
        <div class="form-group">
            <label>password </label>
            <input type="password" class="form-control" id="exampleInputPassword1" name="password" required>
        </div>
        <div class="form-group">
            <small id="emailHelp" class="form-text text-muted" style="color: red;"> ${msg}</small>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
</div>

</body>
</html>
