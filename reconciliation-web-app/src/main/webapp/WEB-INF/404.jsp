<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="icon" href="${pageContext.request.contextPath}/resources/favicon.jpg">
    <title>Page Not found</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-10 col-md-4 col-md-offset-4">
            <div class="alert alert-warning" role="alert">
                <h4 class="alert-heading">Page not found ${requestScope.error.msg}</h4>
                <p>The requested resource is not found </p>
            </div>
        </div>
    </div>
</div>


</body>
</html>