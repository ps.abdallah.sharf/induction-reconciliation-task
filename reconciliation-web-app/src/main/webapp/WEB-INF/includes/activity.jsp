<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

    <div class="row row-cols-2" style="margin-top: 20px">
        <div class="col-sm-2 card-header" >Session Time</div>
        <div class="col-lg-10 card-header" >Reconciliation Activity </div>
        <c:set var="index" value="0"/>
        <c:set var="activityTimelist" value="${sessionScope.activityMap.keySet().toArray()}"/>
        <c:forEach items="${sessionScope.activityMap.values()}" var="activitylist">

            <div class="col-sm-2">
                    ${activityTimelist[index]}
            </div>
            <div class="col-lg-10">
                <table class="table">
                    <thead class=" thead-light">
                    <tr>
                        <th>#</th>
                        <th>reconciliationTime</th>
                        <th>sourceFileName</th>
                        <th>targetFileName</th>
                        <th>matched</th>
                        <th>mismatched</th>
                        <th>missing</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="i" value="1"/>
                    <c:forEach items="${activitylist}" var="activity">
                        <tr>
                            <td>${i}</td>
                            <td>${activity.reconciliationTime}</td>
                            <td>${activity.sourceFileName}</td>
                            <td>${activity.targetFileName}</td>
                            <td>${activity.matched}</td>
                            <td>${activity.mismatched}</td>
                            <td>${activity.missing}</td>
                        </tr>
                        <c:set var="i" value="${i+1}"/>
                    </c:forEach>
                    <c:set var="index" value="${index+1}"/>
                    </tbody>
                </table>
            </div>
        </c:forEach>
    </div>

</div>


</div>

</body>
</html>