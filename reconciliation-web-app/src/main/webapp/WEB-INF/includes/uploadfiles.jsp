<style>

    body {
        background-color: #ffffff;
    }

    #uploadForm {
        background-color: #ffffff;
        margin: 10px;
        font-family: Raleway;
        padding: 20px;
        min-width: 400px;
        height: 100%;
    }

    h1 {
        text-align: center;
    }

    input {
        padding: 10px;
        width: 100%;
        font-size: 17px;
        font-family: Raleway;
        border: 1px solid #aaaaaa;
    }

    .tab2 {
        display: none;
    }
    input.invalid {
        background-color: #ffdddd;
    }
    button {
        background-color: cornflowerblue;
        color: #ffffff;
        border: none;
        padding: 10px 20px;
        font-size: 17px;
        font-family: Raleway;
        cursor: pointer;
    }

    button:hover {
        opacity: 0.8;
    }

    #prevBtn {
        background-color: #bbbbbb;
    }

    select {
        padding: 10px;
        width: 100%;
        font-size: 17px;
        font-family: Raleway;
        border: 1px solid #aaaaaa;
    }

</style>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">


<form id="uploadForm" action="<%=request.getContextPath()%>/compare" method="post" enctype="multipart/form-data">
    <div class="tab2">
        <label> Source Name</label>
        <input placeholder="source name" oninput="this.className = ''" name="sourcename" id="source_name">
        <div>
            <br>
            <label>Source Type</label>
            <br>
            <select name="source_format" id="source_type">
                <option value="csv">csv</option>
                <option value="json">json</option>
            </select>
        </div>
        <div>
            <br>
            <label>File:</label>
            <input type="file" name="sourcefile">
        </div>
    </div>
    <div class="tab2">
        <label> Target Name</label>
        <input placeholder="target name" oninput="this.className = ''" name="targetname" id="target_name">
        <div>
            <br>
            <label>Target Type</label>
            <br>
            <select name="target_format" id="target_type">
                <option value="csv">csv</option>
                <option value="json">json</option>
            </select>
        </div>
        <div>
            <br>
            <label>File:</label>
            <input type="file" name="targetfile">
        </div>
    </div>
    <div class="tab2">
        <div class="container">

            <div class="row">

                <div class="col">
                    <div class="rounded-sm border border-primary">

                        <table class="table">
                            <thead style="background-color: #bbbbbb;color: white">
                            <tr>
                                <th>Source</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Name:</td>
                                <td><span id="src_name"></span></td>
                            </tr>
                            <tr>
                                <td>Type:</td>
                                <td><span id="src_type"></span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col">
                    <div class="rounded-sm border border-primary">
                        <table class="table">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Source</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Name:</td>
                                <td id="tar_name"></td>
                            </tr>
                            <tr>
                                <td>Type:</td>
                                <td id="tar_type"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div style="overflow:auto;">
        <div style="float:right;">
            <button type="button" id="cancelBtn" onclick="move(-2)" style="visibility: hidden">Cancel</button>
            <button type="button" id="prevBtn" onclick="move(-1)">Previous</button>
            <button type="button" id="nextBtn" onclick="move(1)">Next</button>
        </div>
    </div>

</form>

        <script>
            var currentTab = 0;
            showTab(currentTab);

            function showTab(n) {
                var x = document.getElementsByClassName("tab2");
                x[n].style.display = "block";
                if (n == 0) {
                    document.getElementById("prevBtn").style.display = "none";
                } else {
                    document.getElementById("prevBtn").style.display = "inline";
                }
                if (n == (x.length - 1)) {
                    document.getElementById("nextBtn").innerHTML = "Compare";
                } else {
                    document.getElementById("nextBtn").innerHTML = "Next";
                }
            }

            function move(n) {
                var x = document.getElementsByClassName("tab2");
                if (n == 1 && !validateForm()) return false;
                x[currentTab].style.display = "none";
                currentTab = currentTab + n;
                if (currentTab >= x.length) {
                    document.getElementById("uploadForm").submit();
                    return false;
                }
                showTab(currentTab);
                if (currentTab===2){
                    assignTableDetails();
                    showCancelBtn();
                }
            }

            function assignTableDetails() {
                var srcName = document.getElementById("source_name").value;
                var srcType = document.getElementById("source_type");
                var tarName = document.getElementById("target_name").value;
                var tarType = document.getElementById("target_type");
                document.getElementById("src_name").innerHTML = srcName;
                document.getElementById("src_type").innerHTML = srcType.options[srcType.selectedIndex].value;
                document.getElementById("tar_name").innerHTML = tarName;
                document.getElementById("tar_type").innerHTML = tarType.options[tarType.selectedIndex].value;
            }

             function showCancelBtn(){
                 document.getElementById("cancelBtn").style.visibility="visible";
            }

            $("#cancelBtn").click(function (){
                $('#uploadForm')[0].reset();
                document.getElementById("cancelBtn").style.visibility="hidden";
            });

            function validateForm() {
                var x, y, i, valid = true;
                x = document.getElementsByClassName("tab2");
                y = x[currentTab].getElementsByTagName("input");
                for (i = 0; i < y.length; i++) {
                    if (y[i].value == "") {
                        y[i].className += " invalid";
                        valid = false;
                    }
                }
                return valid;
            }
        </script>
    </div>




