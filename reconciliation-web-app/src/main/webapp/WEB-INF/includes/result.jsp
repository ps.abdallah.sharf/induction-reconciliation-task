<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab" style="margin: 1.5em">

    <h3>Reconciliation Result</h3>

    <div class="tab btn-group btn-group-lg ">
        <button class="tablinks active" role="group" onclick="showResult('Match')" style="background-color: skyblue">
            Match
        </button>
        <button class="tablinks" onclick="showResult('Mismatch')" style="background-color: skyblue">Mismatch</button>
        <button class="tablinks" onclick="showResult('Missing')" style="background-color: skyblue">Missing</button>
    </div>

    <div id="Match" class="tabcontent2" style="display: block;margin: 1.5em;min-height: 250px">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>transaction id</th>
                <th>amount</th>
                <th>currency code</th>
                <th>value date</th>
            </tr>
            </thead>
            <tbody>
            <c:set var="i" value="1"/>
            <c:forEach items="${sessionScope.resultMap.get('matching-transactions')}" var="activity">
                <tr>
                    <th>${i}</th>
                    <td>${activity.transactionId}</td>
                    <td>${activity.amount}</td>
                    <td>${activity.currencyCode}</td>
                    <td>${activity.valueDate}</td>
                </tr>
                <c:set var="i" value="${i+1}"/>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <div id="Mismatch" class="tabcontent2" style="display: none;margin: 1.5em;min-height: 250px">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>found in file</th>
                <th>transaction id</th>
                <th>amount</th>
                <th>currency code</th>
                <th>value date</th>
            </tr>
            </thead>
            <tbody>
            <c:set var="i" value="1"/>
            <c:forEach items="${sessionScope.resultMap.get('mismatched-transactions')}" var="activity">
                <tr>
                    <th>${i}</th>
                    <td>${activity.foundInFile}</td>
                    <td>${activity.transactionId}</td>
                    <td>${activity.amount}</td>
                    <td>${activity.currencyCode}</td>
                    <td>${activity.valueDate}</td>
                </tr>
                <c:set var="i" value="${i+1}"/>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <div id="Missing" class="tabcontent2" style="display: none; margin: 1.5em;min-height: 250px">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>found in file</th>
                <th>transaction id</th>
                <th>amount</th>
                <th>currency code</th>
                <th>value date</th>
            </tr>
            </thead>
            <tbody>
            <c:set var="i" value="1"/>
            <c:forEach items="${sessionScope.resultMap.get('missing-transactions')}" var="activity">
                <tr>
                    <th>${i}</th>
                    <td>${activity.foundInFile}</td>
                    <td>${activity.transactionId}</td>
                    <td>${activity.amount}</td>
                    <td>${activity.currencyCode}</td>
                    <td>${activity.valueDate}</td>
                </tr>
                <c:set var="i" value="${i+1}"/>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <jsp:include page="result_bottom_tab.jsp"></jsp:include>
</div>


<script>
    $(document).ready(function () {
        if ("${sessionScope.resultMap.size()}" == 0) {
            $('#nav-contact').empty().append("<p>You haven't done any comparing yet.</p>");
        }
    });

    function showResult( resulttype) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent2");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(resulttype).style.display = "block";
        $(this).addClass("active");
    }
</script>


