<div class="row row-cols-2" >
    <div class="col-md-3"></div>
    <div class="col-md-4">
        <form action="${pageContext.request.contextPath}/compare/compare-new-files">
            <button type="submit">compare new files</button>
        </form>
    </div>
    <div class="col-sm-2">
        <select id="select_box" onchange="download()">
            <option selected disabled >download</option>
            <option value="csv">
                CSV
            </option>
            <option value="json">
                JSON
            </option>
        </select>
    </div>
</div>
<script>
    function download(){
        var sel=document.getElementById("select_box");
        var selectedFormat=sel.options[sel.selectedIndex].value;
        var url='${pageContext.request.contextPath}/download/'+ selectedFormat;
        window.location.href=url;
    }
</script>