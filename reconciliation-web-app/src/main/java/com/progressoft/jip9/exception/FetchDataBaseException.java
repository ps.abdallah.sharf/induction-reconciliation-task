package com.progressoft.jip9.exception;

import java.sql.SQLException;

public class FetchDataBaseException extends RuntimeException {
    public FetchDataBaseException(SQLException exception) {
        super(exception);
    }
}
