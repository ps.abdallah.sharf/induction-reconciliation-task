package com.progressoft.jip9.exception;

import java.sql.SQLException;

public class WriteToDataBaseException extends RuntimeException {
    public WriteToDataBaseException(SQLException exception) {
        super(exception);
    }
}
