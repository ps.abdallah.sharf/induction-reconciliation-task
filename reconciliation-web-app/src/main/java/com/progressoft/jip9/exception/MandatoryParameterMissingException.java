package com.progressoft.jip9.exception;

public class MandatoryParameterMissingException extends RuntimeException {
    public MandatoryParameterMissingException(String message) {
        super(message);
    }
}
