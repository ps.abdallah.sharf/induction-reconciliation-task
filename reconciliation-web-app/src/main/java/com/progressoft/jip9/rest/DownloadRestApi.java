package com.progressoft.jip9.rest;

import com.progressoft.jip9.resultType.TransactionResultType;
import com.progressoft.jip9.utils.TemporaryZippedFilePrinter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.Set;


public class DownloadRestApi extends HttpServlet {

    private final TemporaryZippedFilePrinter zippedFilePrinter;

    public DownloadRestApi(TemporaryZippedFilePrinter zippedFilePrinter) {
        this. zippedFilePrinter = zippedFilePrinter;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try (ServletOutputStream servletOutputStream = resp.getOutputStream()) {
            String format = req.getPathInfo().substring(1);
            Map<String, Set<? extends TransactionResultType>> resultMap =
                    (Map<String, Set<? extends TransactionResultType>>) req.getSession().getAttribute("resultMap");
            File zipResultFile=zippedFilePrinter.printZippedFile(resultMap, format);
            setResponseHeader(resp, zipResultFile);
            printZippedResultFileToUser(servletOutputStream, zipResultFile);
        } catch (IOException exception) {
            resp.sendRedirect(req.getContextPath() + "/error/download_error.html");
            return;

        }
    }

    private void printZippedResultFileToUser(ServletOutputStream servletOutputStream, File zipResultFile) throws IOException {
        try (FileInputStream input = new FileInputStream(zipResultFile);
             BufferedInputStream buf = new BufferedInputStream(input)) {
            int readBytes;
            while ((readBytes = buf.read()) != -1) {
                servletOutputStream.write(readBytes);
            }
        }
    }

    private void setResponseHeader(HttpServletResponse resp, File zipResultFile) {
        String zipContentType = "application/zip";
        String contentDispositionKey = "Content-Disposition";
        String contentDispositionValue = "attachment; filename=\"%s\"";
        resp.setContentType(zipContentType);
        resp.setHeader(contentDispositionKey, String.format(contentDispositionValue, "result_files.zip"));
        resp.setContentLength((int) zipResultFile.length());
    }


}
