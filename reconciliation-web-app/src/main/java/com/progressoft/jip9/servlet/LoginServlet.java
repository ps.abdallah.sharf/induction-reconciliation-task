package com.progressoft.jip9.servlet;

import com.progressoft.jip9.business.absoluteBusinesses.AbsoluteBusinesses;
import com.progressoft.jip9.model.Activity;
import com.progressoft.jip9.model.Session;
import com.progressoft.jip9.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


public class LoginServlet extends HttpServlet {
    private final AbsoluteBusinesses absoluteBusinesses;

    public LoginServlet(AbsoluteBusinesses absoluteBusinesses) {
        this.absoluteBusinesses = absoluteBusinesses;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("userId") != null &&
                req.getSession(false).getAttribute("firstSessionId") != null) {
            resp.sendRedirect(req.getContextPath() + "/compare");
            return;// TODO should return after this
        }
        req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("username");
        String passWord = req.getParameter("password");
        User user = new User(userName, passWord);
        if (!absoluteBusinesses.isValidUser(user)) {
            req.setAttribute("msg", "username or password is incorrect");
            req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
            return;
        }
        req.getSession().setAttribute("userId", userName);
        String sessionId = req.getSession().getId();
        addSessionToDBAndSession(req, sessionId);
        getActivityFromDBAndAddToSession(req, user);
        resp.sendRedirect(req.getContextPath() + "/compare");
    }


    private void getActivityFromDBAndAddToSession(HttpServletRequest req, User user) {
        Map<LocalDateTime, List<Activity>> activityMap = absoluteBusinesses.getActivitiesMap(user);
        req.getSession().setAttribute("activityMap", activityMap);
    }

    private void addSessionToDBAndSession(HttpServletRequest req, String sessionId) {
        Session session = new Session();
        session.setSessionId(sessionId);
        session.setUserId((String) req.getSession().getAttribute("userId"));
        session.setSessionTime(LocalDateTime.now());
        absoluteBusinesses.addSession(session);
        addFirstSessionDetailsToSession(req, session);
    }

    private void addFirstSessionDetailsToSession(HttpServletRequest req, Session session) {
        req.getSession().setAttribute("firstSessionId", session.getSessionId());
        req.getSession().setAttribute("firstSessTime", session.getSessionTime());
    }
}
