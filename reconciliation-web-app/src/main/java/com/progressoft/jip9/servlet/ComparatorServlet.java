package com.progressoft.jip9.servlet;

import com.progressoft.jip9.exception.ParsingTransactionException;
import com.progressoft.jip9.service.ActivitiesCollector;
import com.progressoft.jip9.service.ComparisonServices;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ComparatorServlet extends HttpServlet {

    private final ComparisonServices comparisonServices;
    private final ActivitiesCollector activitiesCollector;

    public ComparatorServlet(ComparisonServices comparisonServices, ActivitiesCollector activitiesCollector) {
        this.comparisonServices=comparisonServices;
        this.activitiesCollector = activitiesCollector;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.changeSessionId();
        String pathInfo = req.getPathInfo();
        if (req.getSession().getAttribute("resultMap") == null
                || "/compare-new-files".equals(pathInfo)) {
            req.getRequestDispatcher("/WEB-INF/views/main_compare.jsp").forward(req, resp);
            return;
        }
        req.getRequestDispatcher("/WEB-INF/views/main_result.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/plain;charset=UTF-8");
        try {
            comparisonServices.doComparingAndAddToSession(request);
            activitiesCollector.prepareActivityAndAddToDBAndSession(request);
            request.changeSessionId();
        }  catch (ParsingTransactionException | IOException | ServletException exception) {
            response.sendRedirect(request.getContextPath()+"/error/reading_file_error.html");
            return;
        }
        request.getRequestDispatcher("/WEB-INF/views/main_result.jsp").forward(request, response);
    }

}
