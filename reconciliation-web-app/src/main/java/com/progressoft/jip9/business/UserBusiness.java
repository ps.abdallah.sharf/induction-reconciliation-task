package com.progressoft.jip9.business;

import com.progressoft.jip9.exception.FetchDataBaseException;
import com.progressoft.jip9.model.User;
import com.progressoft.jip9.service.cryptor.Cryptor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserBusiness {
    private final Cryptor cryptor;

    public UserBusiness(Cryptor cryptor) {
        failedIfNull(cryptor, "cryptor");
        this.cryptor = cryptor;
    }

    public boolean isValidUser(DataSource dataSource, User user) {
        failedIfAnyIsInvalid(dataSource, user);
        boolean isValidUser;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from rec_user where user_id=?")) {
            preparedStatement.setString(1, user.getUserName());
            isValidUser = isValidUser(user, preparedStatement);
        } catch (SQLException exception) {
            throw new FetchDataBaseException(exception);
        }
        return isValidUser;
    }

    private void failedIfAnyIsInvalid(DataSource dataSource, User user) {
        failedIfNull(dataSource, "dataSource");
        failedIfNull(user, "user");
    }

    private void failedIfNull(Object object, String name) {
        if (object == null)
            throw new NullPointerException(name + " is null");
    }

    private boolean isValidUser(User user, PreparedStatement preparedStatement) throws SQLException {
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            if (resultSet.next()) {
                String hashed = resultSet.getString(2);
                return cryptor.checkHash(user.getPassword(), hashed);
            }
        }
        return false;
    }

}
