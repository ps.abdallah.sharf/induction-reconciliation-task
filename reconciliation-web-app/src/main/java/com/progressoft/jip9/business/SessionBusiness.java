package com.progressoft.jip9.business;

import com.progressoft.jip9.exception.WriteToDataBaseException;
import com.progressoft.jip9.model.Session;
import com.progressoft.jip9.model.User;

import javax.sql.DataSource;
import java.sql.*;


public class SessionBusiness {

    public void addSession(DataSource dataSource, Session session) {
        failedIfAnyIsInvalid(dataSource,session);
        String sqlQuery = "insert into rec_session values (?,?,?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {
            preparedStatement.setString(1, session.getSessionId());
            preparedStatement.setString(2, session.getUserId());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(session.getSessionTime()));
            preparedStatement.execute();
        } catch (SQLException exception) {
            throw new WriteToDataBaseException(exception);
        }
    }

    private void failedIfAnyIsInvalid(DataSource dataSource, Session session) {
        failedIfNull(dataSource,"dataSource");
        failedIfNull(session,"session");
    }

    private void failedIfNull(Object object, String name) {
        if (object==null)
            throw new NullPointerException(name + " is null");
    }
}
