package com.progressoft.jip9.business.absoluteBusinesses;

import com.progressoft.jip9.model.Activity;
import com.progressoft.jip9.model.Session;
import com.progressoft.jip9.model.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface AbsoluteBusinesses {

    boolean isValidUser(User user);

    void addSession(Session session);

    void addActivity(Activity activity);

    Map<LocalDateTime, List<Activity>> getActivitiesMap(User user);
}
