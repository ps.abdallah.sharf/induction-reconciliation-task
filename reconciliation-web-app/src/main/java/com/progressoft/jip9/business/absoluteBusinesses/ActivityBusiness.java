package com.progressoft.jip9.business.absoluteBusinesses;

import com.progressoft.jip9.exception.FetchDataBaseException;
import com.progressoft.jip9.exception.WriteToDataBaseException;
import com.progressoft.jip9.model.Activity;
import com.progressoft.jip9.model.User;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

public class ActivityBusiness {
    public void addActivity(DataSource dataSource, Activity activity) {
        failedIfNull(dataSource, "dataSource");
        failedIfNull(activity, "activity");
        String sqlQuery = "insert into rec_activity values (default ,?,?,?,?,?,?,?,?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {
            addActivityFieldsToStatement(activity, preparedStatement);
            preparedStatement.execute();
        } catch (SQLException exception) {
            throw new WriteToDataBaseException(exception);
        }
    }

    public Map<LocalDateTime, List<Activity>> getActivitiesMap(DataSource dataSource, User user) {
        failedIfNull(dataSource, "dataSource");
        failedIfNull(user, "user");
        Map<LocalDateTime, List<Activity>> activityMap = new TreeMap<>();
        String query = "SELECT rec_session.session_time, rec_activity.* from rec_session inner join" +
                " rec_activity On rec_activity.session_id=rec_session.session_id where rec_session.user_id=?;";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, user.getUserName());
            getActivityMapFromResultSet(activityMap, preparedStatement);
        } catch (SQLException exception) {
            throw new FetchDataBaseException(exception);
        }
        return activityMap;
    }

    private void getActivityMapFromResultSet(Map<LocalDateTime, List<Activity>> activityMap, PreparedStatement preparedStatement) throws SQLException {
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                LocalDateTime sessionTime = resultSet.getTimestamp("session_time").toLocalDateTime();
                activityMap.putIfAbsent(sessionTime, new ArrayList<>());
                Activity activity = getActivityFromResultSet(resultSet);
                activityMap.get(sessionTime).add(activity);
            }
        }
    }

    private Activity getActivityFromResultSet(ResultSet resultSet) throws SQLException {
        Activity activity = new Activity();
        activity.setSessionId(resultSet.getString("session_id"));
        activity.setReconciliationTime(resultSet.getTimestamp("rec_time").toLocalDateTime());
        activity.setSourceFileName(resultSet.getString("src_file"));
        activity.setTargetFileName(resultSet.getString("tar_file"));
        activity.setMatched(resultSet.getInt("matched"));
        activity.setMismatched(resultSet.getInt("mismatched"));
        activity.setMissing(resultSet.getInt("missing"));
        return activity;
    }

    private void addActivityFieldsToStatement(Activity activity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, activity.getUserId());
        preparedStatement.setString(2, activity.getSessionId());
        preparedStatement.setTimestamp(3, Timestamp.valueOf(activity.getReconciliationTime()));
        preparedStatement.setString(4, activity.getSourceFileName());
        preparedStatement.setString(5, activity.getTargetFileName());
        preparedStatement.setInt(6, activity.getMatched());
        preparedStatement.setInt(7, activity.getMismatched());
        preparedStatement.setInt(8, activity.getMissing());
    }


    private void failedIfNull(Object object, String name) {
        if (object == null)
            throw new NullPointerException(name + " is null");
    }
}
