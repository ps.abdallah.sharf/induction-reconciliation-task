package com.progressoft.jip9.business;

import com.progressoft.jip9.business.absoluteBusinesses.AbsoluteBusinesses;
import com.progressoft.jip9.business.absoluteBusinesses.ActivityBusiness;
import com.progressoft.jip9.model.Activity;
import com.progressoft.jip9.model.Session;
import com.progressoft.jip9.model.User;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class BusinessesWrapper implements AbsoluteBusinesses {

    private DataSource dataSource;
    private SessionBusiness sessionBusiness;
    private UserBusiness business;
    private ActivityBusiness activityBusiness;

    private BusinessesWrapper() {
    }

    public static DataSourceSetter build() {
        return new BusinessesWrapperBuilder();
    }


    public boolean isValidUser(User user) {
        return business.isValidUser(dataSource, user);
    }

    public void addSession(Session session) {
        sessionBusiness.addSession(dataSource, session);
    }

    public void addActivity( Activity activity) {
        activityBusiness.addActivity(dataSource, activity);
    }

    public Map<LocalDateTime, List<Activity>> getActivitiesMap(User user) {
        return activityBusiness.getActivitiesMap(dataSource, user);
    }


    public static class BusinessesWrapperBuilder implements UserBusinessSetter
            , SessionBusinessSetter, DataSourceSetter {
        BusinessesWrapper businessesWrapper;

        private BusinessesWrapperBuilder() {
            this.businessesWrapper = new BusinessesWrapper();
        }

        @Override
        public UserBusinessSetter setDataSource(DataSource dataSource) {
            businessesWrapper.dataSource = dataSource;
            return this;
        }

        @Override
        public BusinessesWrapperBuilder setSessionBusiness(SessionBusiness sessionBusiness) {
            businessesWrapper.sessionBusiness = sessionBusiness;
            return this;
        }

        @Override
        public SessionBusinessSetter setUserBusiness(UserBusiness business) {
            businessesWrapper.business = business;
            return this;

        }

        public BusinessesWrapper setActivityBusiness(ActivityBusiness activityBusiness) {
            businessesWrapper.activityBusiness = activityBusiness;
            return businessesWrapper;
        }

    }

    public interface DataSourceSetter {
        UserBusinessSetter setDataSource(DataSource dataSource);
    }

    public interface UserBusinessSetter {
        SessionBusinessSetter setUserBusiness(UserBusiness business);
    }

    public interface SessionBusinessSetter {
        BusinessesWrapperBuilder setSessionBusiness(SessionBusiness sessionBusiness);
    }
}
