package com.progressoft.jip9.initializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.jip9.FileParserFactory;
import com.progressoft.jip9.ParserSelector;
import com.progressoft.jip9.business.BusinessesWrapper;
import com.progressoft.jip9.business.SessionBusiness;
import com.progressoft.jip9.business.UserBusiness;
import com.progressoft.jip9.business.absoluteBusinesses.AbsoluteBusinesses;
import com.progressoft.jip9.business.absoluteBusinesses.ActivityBusiness;
import com.progressoft.jip9.comparator.ComparatorFactory;
import com.progressoft.jip9.comparator.DefaultComparatorFactory;
import com.progressoft.jip9.filter.DownloadResultApiFilter;
import com.progressoft.jip9.filter.LoggingFieldsCheckFilter;
import com.progressoft.jip9.filter.LoginCheckFilter;
import com.progressoft.jip9.rest.DownloadRestApi;
import com.progressoft.jip9.service.ActivitiesCollector;
import com.progressoft.jip9.service.ComparisonServices;
import com.progressoft.jip9.service.cryptor.BCryptHasher;
import com.progressoft.jip9.service.cryptor.Cryptor;
import com.progressoft.jip9.servlet.ComparatorServlet;
import com.progressoft.jip9.servlet.LoginServlet;
import com.progressoft.jip9.servlet.LogoutServlet;
import com.progressoft.jip9.utils.TemporaryZippedFilePrinter;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.sql.DataSource;
import java.util.EnumSet;
import java.util.Set;

public class ReconciliationInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        ParserSelector parserSelector = new FileParserFactory();
        ComparatorFactory comparatorFactory = new DefaultComparatorFactory(parserSelector);
        AbsoluteBusinesses absoluteBusinesses = getBusinessesWrapper(ctx);
        Gson gson = getGsonInstance();
        registerLoginServlet(ctx, absoluteBusinesses);
        registerLogoutServlet(ctx);
        registerComparatorServlet(ctx, absoluteBusinesses, comparatorFactory);
        registerDownloadRestApiServlet(ctx, gson);
        registerLoggingFieldsCheckFilter(ctx);
        registerLoginCheckFilter(ctx);
        registerDownloadResultApiFilter(ctx);
    }

    private void registerLoginCheckFilter(ServletContext ctx) {
        LoginCheckFilter filter = new LoginCheckFilter();
        FilterRegistration.Dynamic domainCheckFilter = ctx.addFilter("loginCheckFilter", filter);
        domainCheckFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
    }

    private void registerLoggingFieldsCheckFilter(ServletContext ctx) {
        LoggingFieldsCheckFilter filter = new LoggingFieldsCheckFilter();
        FilterRegistration.Dynamic registration = ctx.addFilter("loggingFieldsCheckFilter", filter);
        registration.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/login");
    }

    private void registerDownloadRestApiServlet(ServletContext ctx, Gson gson) {
        TemporaryZippedFilePrinter zippedFilePrinter = new TemporaryZippedFilePrinter(gson);
        DownloadRestApi resetApi = new DownloadRestApi(zippedFilePrinter);
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("downloadRestApi", resetApi);
        servletRegistration.addMapping("/download/*");
    }

    private void registerDownloadResultApiFilter(ServletContext ctx) {
        DownloadResultApiFilter filter = new DownloadResultApiFilter();
        FilterRegistration.Dynamic registration = ctx.addFilter("downloadResultApiFilter", filter);
        registration.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/download/*");
    }

    private void registerComparatorServlet(ServletContext ctx, AbsoluteBusinesses absoluteBusinesses, ComparatorFactory comparatorFactory) {
        ActivitiesCollector activitiesCollector = new ActivitiesCollector(absoluteBusinesses);
        ComparisonServices comparisonServices = new ComparisonServices(comparatorFactory);
        ComparatorServlet comparatorServlet = new ComparatorServlet(comparisonServices, activitiesCollector);
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("comparatorServlet", comparatorServlet);
        servletRegistration.setMultipartConfig(new MultipartConfigElement("/tmp", 1024 * 1024 * 30, 1024 * 1024 * 35, 1024 * 1024));
        servletRegistration.addMapping("/compare/*");
    }

    private void registerLoginServlet(ServletContext ctx, AbsoluteBusinesses absoluteBusinesses) {
        LoginServlet servlet = new LoginServlet(absoluteBusinesses);
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("loginServlet", servlet);
        servletRegistration.addMapping("/login");
    }

    private void registerLogoutServlet(ServletContext ctx) {
        LogoutServlet servlet = new LogoutServlet();
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("logoutServlet", servlet);
        servletRegistration.addMapping("/logout");
    }


    private BusinessesWrapper getBusinessesWrapper(ServletContext ctx) throws ServletException {
        DataSource dataSource = getDataSource(ctx);
        Cryptor cryptor = new BCryptHasher();
        UserBusiness userBusiness = new UserBusiness(cryptor);
        SessionBusiness sessionBusiness = new SessionBusiness();
        ActivityBusiness activityBusiness = new ActivityBusiness();
        return BusinessesWrapper.build().setDataSource(dataSource)
                .setUserBusiness(userBusiness).setSessionBusiness(sessionBusiness).setActivityBusiness(activityBusiness);
    }

    private Gson getGsonInstance() {
        GsonBuilder gb = new GsonBuilder();
        gb.setPrettyPrinting();
        return gb.create();
    }

    private DataSource getDataSource(ServletContext ctx) throws ServletException {
        String dbURL = ctx.getInitParameter("db-url");
        String username = ctx.getInitParameter("username");
        String password = ctx.getInitParameter("password");
        if (StringUtils.isEmpty(dbURL))
            throw new ServletException("db-url context parameter is not defined");
//        return new DefaultDataSource(dbURL, username, password).getDataSource();
        return null; // TODO this class is not committed
    }
}
