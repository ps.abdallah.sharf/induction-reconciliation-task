package com.progressoft.jip9.filter;

import com.progressoft.jip9.resultType.TransactionResultType;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

// TODO this is preferred to be a servlet
public class DownloadResultApiFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        HttpServletResponse httpRes = (HttpServletResponse) servletResponse;
        String format = httpReq.getPathInfo();
        if (StringUtils.isEmpty(format) || (!format.equals("/csv") && !format.equals("/json"))) {
            httpRes.sendRedirect(httpReq.getContextPath() + "/error/invalid_format_error.html");
            return;
        }
        Map<String, Set<? extends TransactionResultType>> resultMap =
                (Map<String, Set<? extends TransactionResultType>>) httpReq.getSession().getAttribute("resultMap");
        if (resultMap == null) {
            httpRes.sendRedirect(httpReq.getContextPath() + "/error/no_result_error.html");
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
