package com.progressoft.jip9.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class LoginCheckFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        HttpSession session = httpReq.getSession(false);
        boolean isLoggedIn = session != null && session.getAttribute("userId") != null;

        if (!isLoggedIn && !httpReq.getServletPath().equals("/login")) {
            HttpServletResponse httpResp = (HttpServletResponse) servletResponse;
            httpReq.setAttribute("err", "You haven't logged in to the system");
            // TODO use redirect instead
            httpReq.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(httpReq, httpResp);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
