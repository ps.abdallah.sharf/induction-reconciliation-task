package com.progressoft.jip9.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;


public class LoggingFieldsCheckFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        String userName = httpReq.getParameter("username");
        String passWord = httpReq.getParameter("password");
        String method = httpReq.getMethod();
        // TODO this should be enhanced to capture all methods, and you should use redirect here to avoid sending post data to index page
        if (method.equals("POST") && StringUtils.isAnyEmpty(userName, passWord)) {
            HttpServletResponse httpResp = (HttpServletResponse) servletResponse;
            httpReq.setAttribute("msg", "You should fill all fields");
            httpReq.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(httpReq, httpResp);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
