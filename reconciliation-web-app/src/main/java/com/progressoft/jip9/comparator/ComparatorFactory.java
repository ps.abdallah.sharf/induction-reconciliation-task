package com.progressoft.jip9.comparator;

import com.progressoft.jip9.TransactionProcessor;

public interface ComparatorFactory {

    TransactionProcessor getComparatorInstance(String sourceFormat, String targetFormat);

}
