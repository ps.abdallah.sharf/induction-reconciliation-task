package com.progressoft.jip9.comparator;

import com.progressoft.jip9.DefaultProcessor;
import com.progressoft.jip9.ParserSelector;
import com.progressoft.jip9.TransactionParser;
import com.progressoft.jip9.TransactionProcessor;

public class DefaultComparatorFactory implements ComparatorFactory {
    private final ParserSelector parserSelector;

    public DefaultComparatorFactory(ParserSelector parserSelector) {
        failedIfNull(parserSelector,"parserSelector");
        this.parserSelector = parserSelector;
    }

    @Override
    public TransactionProcessor getComparatorInstance(String sourceFormat, String targetFormat) {
        failedIfInvalid(sourceFormat,"source format");
        failedIfInvalid(targetFormat,"target format");
        TransactionParser sourceParser = parserSelector.getProperParser(sourceFormat);
        TransactionParser targetParser = parserSelector.getProperParser(targetFormat);
        return new DefaultProcessor(sourceParser, targetParser);
    }

    private void failedIfInvalid(String format, String name) {
        failedIfNull(format,name);
        if (format.trim().isEmpty()){
            throw new IllegalArgumentException(name+" is Empty");
        }

    }

    private void failedIfNull(Object object,String name) {
        if (object==null)
            throw new NullPointerException(name + " is null");
    }
}
