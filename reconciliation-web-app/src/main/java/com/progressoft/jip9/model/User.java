package com.progressoft.jip9.model;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {

    private final String password;
    private final String userName;

    public User() {
        this("", "");
    }

    public User(String userName, String password) {
        this.password = password;
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(password, user.password) &&
                Objects.equals(userName, user.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(password, userName);
    }
}
