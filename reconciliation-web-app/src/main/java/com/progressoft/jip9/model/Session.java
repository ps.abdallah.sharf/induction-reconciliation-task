package com.progressoft.jip9.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;


public class Session implements Serializable {

    private String sessionId;
    private String userId;
    private LocalDateTime sessionTime;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LocalDateTime getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(LocalDateTime sessionTime) {
        this.sessionTime = sessionTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return Objects.equals(sessionId, session.sessionId) &&
                Objects.equals(userId, session.userId) &&
                Objects.equals(sessionTime, session.sessionTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionId, userId, sessionTime);
    }
}
