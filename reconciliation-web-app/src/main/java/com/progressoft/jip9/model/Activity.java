package com.progressoft.jip9.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Activity implements Serializable {

    private long id;
    private String userId;
    private String sessionId;
    private LocalDateTime reconciliationTime;
    private String sourceFileName;
    private String targetFileName;
    private int matched;
    private int mismatched;
    private int missing;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public LocalDateTime getReconciliationTime() {
        return reconciliationTime;
    }

    public void setReconciliationTime(LocalDateTime reconciliationTime) {
        this.reconciliationTime = reconciliationTime;
    }

    public String getSourceFileName() {
        return sourceFileName;
    }

    public void setSourceFileName(String sourceFileName) {
        this.sourceFileName = sourceFileName;
    }

    public String getTargetFileName() {
        return targetFileName;
    }

    public void setTargetFileName(String targetFileName) {
        this.targetFileName = targetFileName;
    }

    public int getMatched() {
        return matched;
    }

    public void setMatched(int matched) {
        this.matched = matched;
    }

    public int getMismatched() {
        return mismatched;
    }

    public void setMismatched(int mismatched) {
        this.mismatched = mismatched;
    }

    public int getMissing() {
        return missing;
    }

    public void setMissing(int missing) {
        this.missing = missing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Activity activity = (Activity) o;
        return id == activity.id &&
                matched == activity.matched &&
                mismatched == activity.mismatched &&
                missing == activity.missing &&
                Objects.equals(userId, activity.userId) &&
                Objects.equals(sessionId, activity.sessionId) &&
                Objects.equals(reconciliationTime, activity.reconciliationTime) &&
                Objects.equals(sourceFileName, activity.sourceFileName) &&
                Objects.equals(targetFileName, activity.targetFileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, sessionId, reconciliationTime, sourceFileName, targetFileName, matched, mismatched, missing);
    }
}
