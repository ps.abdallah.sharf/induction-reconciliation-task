package com.progressoft.jip9.utils;

import com.google.gson.Gson;
import com.progressoft.jip9.exception.IllegalFormatException;
import com.progressoft.jip9.resultType.MatchingTransactionResult;
import com.progressoft.jip9.resultType.NonMatchingTransaction;
import com.progressoft.jip9.resultType.TransactionResultType;

import java.io.*;
import java.nio.file.Files;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class TemporaryZippedFilePrinter {

    private static final String NON_MATCHING_TRANSACTION_HEADER = "found in file,transaction id,amount,currency code,value date";
    private static final String MATCHING_TRANSACTION_HEADER = "transaction id,amount,currency code,value date";
    private final Gson gson;

    public TemporaryZippedFilePrinter(Gson gson) {
        this.gson = gson;
    }

    public File printZippedFile(Map<String, Set<? extends TransactionResultType>> resultMap, String format) throws IOException {
            File zipResultFile = Files.createTempFile("archive", "zip").toFile();
        try (FileOutputStream outputStream = new FileOutputStream(zipResultFile);
             ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream)) {
            if (format.equals("csv")) {
                printAsCSVFormat(resultMap, zipOutputStream);
            } else if (format.equals("json")) {
                printAsJSONFormat(resultMap, zipOutputStream);
            }else {
                throw new IllegalFormatException();
            }
            return zipResultFile;
    }}



    private void printAsCSVFormat(Map<String, Set<? extends TransactionResultType>> resultMap, ZipOutputStream zos) throws IOException {
        for (String key : resultMap.keySet()) {
            zos.putNextEntry(new ZipEntry(key + ".csv"));
            if (key.equals("matching-transactions")) {
                printCsvMatchingTable(resultMap, zos, key);
            } else {
                printCsvNonMatchingTable(resultMap, zos, key);
            }
            zos.closeEntry();
        }


    }

    private void printAsJSONFormat(Map<String, Set<? extends TransactionResultType>> resultMap, ZipOutputStream zos) throws IOException {
        for (String key : resultMap.keySet()) {
            zos.putNextEntry(new ZipEntry(key + ".json"));
            String entity = gson.toJson(resultMap.get(key));
            zos.write(entity.getBytes());
            zos.flush();
        }
        zos.closeEntry();
    }


    private void printCsvNonMatchingTable(Map<String, Set<? extends TransactionResultType>> resultMap, ZipOutputStream writer, String key) throws IOException {
        writeLineIntoFile(writer, NON_MATCHING_TRANSACTION_HEADER);
        for (TransactionResultType result : resultMap.get(key)) {
            NonMatchingTransaction transaction = (NonMatchingTransaction) result;
            StringBuilder line = new StringBuilder(transaction.getFoundInFile().name()).append(",");
            extractAndAppendCommonFields(line, transaction);
            writeLineIntoFile(writer, line.toString());
        }
    }

    private void printCsvMatchingTable(Map<String, Set<? extends TransactionResultType>> resultMap, ZipOutputStream writer, String key) throws IOException {
        writeLineIntoFile(writer, MATCHING_TRANSACTION_HEADER);
        for (TransactionResultType result : resultMap.get(key)) {
            StringBuilder line = new StringBuilder();
            extractAndAppendCommonFields(line, (MatchingTransactionResult) result);
            writeLineIntoFile(writer, line.toString());
        }
    }

    private void writeLineIntoFile(OutputStream writer, String line) throws IOException {
        writer.write(line.getBytes());
        writer.write("\n".getBytes());
        writer.flush();

    }

    private void extractAndAppendCommonFields(StringBuilder line, TransactionResultType transaction) {
        line.append(transaction.getTransactionId()).append(",")
                .append(transaction.getAmount().toString()).append(",")
                .append(transaction.getCurrencyCode()).append(",")
                .append(transaction.getValueDate().toString());
    }

}
