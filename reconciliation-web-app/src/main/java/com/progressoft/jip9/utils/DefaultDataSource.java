package com.progressoft.jip9.utils;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

public class DefaultDataSource {
    private static final HikariConfig configuration = new HikariConfig();
    private final DataSource hikariDataSource;

    {
        configuration.addDataSourceProperty("cachePrepStmts", "true");
        configuration.addDataSourceProperty("prepStmtCacheSize", "250");
        configuration.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        configuration.setAutoCommit(true);
    }

    public DefaultDataSource(String jdbcUrl, String username, String password) {
        configuration.setJdbcUrl(jdbcUrl + "&serverTimezone=UTC");
        configuration.setUsername(username);
        configuration.setPassword(password);
        hikariDataSource = new HikariDataSource(configuration);
    }

    public DataSource getDataSource() {
        return hikariDataSource;
    }


}
