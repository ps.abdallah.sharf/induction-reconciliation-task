package com.progressoft.jip9.service.cryptor;

import org.mindrot.jbcrypt.BCrypt;

public class BCryptHasher implements Cryptor {

    public String hash(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(5));
    }

    public boolean checkHash(String password, String hashed) {
        return BCrypt.checkpw(password, hashed);
    }

}