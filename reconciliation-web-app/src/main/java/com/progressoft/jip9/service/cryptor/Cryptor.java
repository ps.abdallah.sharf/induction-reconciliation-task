package com.progressoft.jip9.service.cryptor;

public interface Cryptor {
     String hash(String password) ;

     boolean checkHash(String password, String hashed);
}
