package com.progressoft.jip9.service;

import com.progressoft.jip9.TransactionProcessor;
import com.progressoft.jip9.business.absoluteBusinesses.AbsoluteBusinesses;
import com.progressoft.jip9.comparator.ComparatorFactory;
import com.progressoft.jip9.model.Activity;
import com.progressoft.jip9.resultType.TransactionResultType;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

public class ComparisonServices {

    private final ComparatorFactory comparatorFactory;

    public ComparisonServices(ComparatorFactory comparatorFactory ) {
        this.comparatorFactory = comparatorFactory;
    }

    public void doComparingAndAddToSession(HttpServletRequest request) throws IOException, ServletException {
        Map<String, Set<? extends TransactionResultType>> resultMap = getComparingResultMap(request);
        request.getSession(true).setAttribute("resultMap", resultMap);

    }


    private Map<String, Set<? extends TransactionResultType>> getComparingResultMap(HttpServletRequest request) throws IOException, ServletException {
        Part sourcePart = request.getPart("sourcefile");
        Part targetPart = request.getPart("targetfile");
        try (InputStream sourceInputStream = sourcePart.getInputStream();
             InputStream targetInputStream = targetPart.getInputStream()) {
            String sourceFormat = request.getParameter("source_format");
            String targetFormat = request.getParameter("target_format");
            TransactionProcessor processor = comparatorFactory.getComparatorInstance(sourceFormat, targetFormat);
            return processor.process(sourceInputStream, targetInputStream);
        }
    }

}
