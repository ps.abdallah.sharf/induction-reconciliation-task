package com.progressoft.jip9.service;

import com.progressoft.jip9.business.absoluteBusinesses.AbsoluteBusinesses;
import com.progressoft.jip9.comparator.ComparatorFactory;
import com.progressoft.jip9.model.Activity;
import com.progressoft.jip9.model.User;
import com.progressoft.jip9.resultType.TransactionResultType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

public class ActivitiesCollector implements Serializable {

    private final AbsoluteBusinesses absoluteBusinesses;

    public ActivitiesCollector(AbsoluteBusinesses absoluteBusinesses) {
        this.absoluteBusinesses = absoluteBusinesses;
    }


    public void prepareActivityAndAddToDBAndSession(HttpServletRequest request) {
        Activity activity = getActivity(request);
        storeActivityToSession(request, activity);
        absoluteBusinesses.addActivity(activity);
    }
    
    private Map<LocalDateTime,List<Activity>> storeActivityToSession(HttpServletRequest req, Activity activity) {
        HttpSession session = req.getSession(true);
        Map<LocalDateTime,List<Activity>> activityMap = (Map<LocalDateTime, List<Activity>>) session.getAttribute("activityMap");
        LocalDateTime firstSessTime = (LocalDateTime) session.getAttribute("firstSessTime");
        activityMap.putIfAbsent(firstSessTime,new ArrayList<>());
        activityMap.get(firstSessTime).add(activity);
        session.setAttribute("activityMap", activityMap);
        return Collections.unmodifiableMap(activityMap);
    }


    private Activity getActivity(HttpServletRequest request) {
        String sourceFileName=  request.getParameter("sourcename");
        String targetFileName= request.getParameter("targetname");
        String firstSessionId = (String) request.getSession().getAttribute("firstSessionId");
        String userId = (String) request.getSession().getAttribute("userId");
        Activity activity = new Activity();
        activity.setUserId(userId);
        activity.setSessionId(firstSessionId);
        activity.setReconciliationTime(LocalDateTime.now());
        activity.setSourceFileName(sourceFileName);
        activity.setTargetFileName(targetFileName);
        setReconciliationDetails(request, activity);
        return activity;
    }

    private void setReconciliationDetails(HttpServletRequest request, Activity activity) {
        Map<String, Set<? extends TransactionResultType>> resultMap =
                (Map<String, Set<? extends TransactionResultType>>) request.getSession().getAttribute("resultMap");
        activity.setMatched(resultMap.get("matching-transactions").size());
        activity.setMismatched(resultMap.get("mismatched-transactions").size());
        activity.setMissing(resultMap.get("missing-transactions").size());
    }
}
