package com.progressoft.jip9;

import com.progressoft.jip9.exception.IllegalTransactionResultException;
import com.progressoft.jip9.resultType.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

public class CSVResultPrinterTest {
    private static Map<String, List<? extends TransactionResultType>> expectedResultsMap;

    @BeforeAll
    static void initializeExpectedResultsMap() {
        expectedResultsMap = new HashMap<>();
        List<MatchingTransactionResult> matchingTransList = new ArrayList<>();
        matchingTransList.add(new MatchingTransactionResult("TR-47884222201", new BigDecimal("140"), "USD",
                LocalDate.of(2020, 1, 20)));
        matchingTransList.add(new MatchingTransactionResult("TR-47884222203", new BigDecimal("5000"), "JOD",
                LocalDate.of(2020, 1, 25)));
        matchingTransList.add(new MatchingTransactionResult("TR-47884222206", new BigDecimal("500"), "USD",
                LocalDate.of(2020, 2, 10)));
        expectedResultsMap.put("matching-transactions", matchingTransList);
        List<MisMatchingTransactionResult> misMatchingTransList = new ArrayList<>();
        misMatchingTransList.add(new MisMatchingTransactionResult("TR-47884222202", new BigDecimal("20"), "JOD",
                LocalDate.of(2020, 1, 22), FoundInFileOption.SOURCE));
        misMatchingTransList.add(new MisMatchingTransactionResult("TR-47884222202", new BigDecimal("30"), "JOD",
                LocalDate.of(2020, 1, 22), FoundInFileOption.TARGET));
        misMatchingTransList.add(new MisMatchingTransactionResult("TR-47884222205", new BigDecimal("60"), "JOD",
                LocalDate.of(2020, 2, 2), FoundInFileOption.SOURCE));
        misMatchingTransList.add(new MisMatchingTransactionResult("TR-47884222205", new BigDecimal("60"), "JOD",
                LocalDate.of(2020, 2, 3), FoundInFileOption.TARGET));
        expectedResultsMap.put("mismatched-transactions", misMatchingTransList);
        List<MissingTransactionResult> MissingTransList = new ArrayList<>();
        MissingTransList.add(new MissingTransactionResult("TR-47884222204", new BigDecimal("1200"), "JOD",
                LocalDate.of(2020, 1, 31), FoundInFileOption.SOURCE));
        MissingTransList.add(new MissingTransactionResult("TR-47884222217", new BigDecimal("12000"), "JOD",
                LocalDate.of(2020, 2, 14), FoundInFileOption.TARGET));
        MissingTransList.add(new MissingTransactionResult("TR-47884222245", new BigDecimal("420"), "USD",
                LocalDate.of(2020, 1, 12), FoundInFileOption.TARGET));
        expectedResultsMap.put("missing-transactions", MissingTransList);
    }

    @Test
    public void givenNullInputs_whenConstruct_thenExceptionThrown() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new CSVResultPrinter(null));
        Assertions.assertEquals("destination directory is null", exception.getMessage());
    }

    @Test
    public void givenNullInputs_whenPrintResult_thenExceptionThrown() throws IOException {
        String distention = Files.createTempDirectory("CSV-result-printer-DummyPath").toString();
        InputStream sourceInputStream = Mockito.mock(InputStream.class);
        InputStream targetInputStream = Mockito.mock(InputStream.class);
        CSVResultPrinter csvResultPrinter = new CSVResultPrinter(distention);
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> csvResultPrinter.printResult(null, sourceInputStream, targetInputStream));
        Assertions.assertEquals("The processor is null is null", exception.getMessage());
    }

    @Test
    public void givenInvalidData_whenPrintResult_thenExceptionThrown() throws IOException {
        String distention = Files.createTempDirectory("CSV-result-printer-DummyPath").toString();
        InputStream sourceInputStream = Mockito.mock(InputStream.class);
        InputStream targetInputStream = Mockito.mock(InputStream.class);
        CSVResultPrinter csvResultPrinter = new CSVResultPrinter(distention);
        TransactionProcessor processor = (p1, p2, biConsumer) -> processInvalidData(biConsumer);
        IllegalTransactionResultException exception = Assertions.assertThrows(IllegalTransactionResultException.class,
                () -> csvResultPrinter.printResult(processor, sourceInputStream, targetInputStream));
        Assertions.assertEquals("Undefined result type: invalid-transactions-result", exception.getMessage());
    }

    @Test
    public void givenValidInputs_whenPrintResults_thenResultsAsExpected() throws IOException {
        Path destinationPath = Files.createTempDirectory("reconciliation-results");
        CSVResultPrinter printer = new CSVResultPrinter(destinationPath.toString());
        InputStream inputStream = Mockito.mock(InputStream.class);
        TransactionProcessor processor = (p1, p2, biConsumer) -> doMockProcess(biConsumer);
        Assertions.assertDoesNotThrow(() -> printer.printResult(processor, inputStream, inputStream));
        Assertions.assertIterableEquals(expectedResultsMap.get("matching-transactions"), getActualMatchingResultList(destinationPath));
        Assertions.assertIterableEquals(expectedResultsMap.get("mismatched-transactions"), getActualMisMatchingResultsList(destinationPath));
        Assertions.assertIterableEquals(expectedResultsMap.get("missing-transactions"), getActualMissingTransList(destinationPath));
    }

    private List<MatchingTransactionResult> getActualMatchingResultList(Path destinationPath) {
        Path path = Paths.get(destinationPath+"/reconciliation-results/matching-transactions.csv");
        List<MatchingTransactionResult> resultList = new ArrayList<>();
        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            skipHeader(bufferedReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] lineArray = line.split(",");
                MatchingTransactionResult transaction = new MatchingTransactionResult();
                transaction.setTransactionId(lineArray[0]);
                transaction.setAmount(new BigDecimal(lineArray[1]));
                transaction.setCurrencyCode(lineArray[2]);
                transaction.setValueDate(LocalDate.parse(lineArray[3]));
                resultList.add(transaction);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return resultList;
    }


    private List<MisMatchingTransactionResult> getActualMisMatchingResultsList(Path destinationPath) {
        Path path = Paths.get(destinationPath+"/reconciliation-results/mismatched-transactions.csv");
        List<MisMatchingTransactionResult> resultList = new ArrayList<>();
        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            skipHeader(bufferedReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] lineArray = line.split(",");
                MisMatchingTransactionResult transaction = new MisMatchingTransactionResult();
                transaction.setFoundInFile(FoundInFileOption.getInstance(lineArray[0]));
                transaction.setTransactionId(lineArray[1]);
                transaction.setAmount(new BigDecimal(lineArray[2]));
                transaction.setCurrencyCode(lineArray[3]);
                transaction.setValueDate(LocalDate.parse(lineArray[4]));
                resultList.add(transaction);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return resultList;
    }

    private List<MissingTransactionResult> getActualMissingTransList(Path destinationPath) {
        Path path = Paths.get(destinationPath+"/reconciliation-results/missing-transactions.csv");
        List<MissingTransactionResult> resultList = new ArrayList<>();
        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            skipHeader(bufferedReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] lineArray = line.split(",");
                MissingTransactionResult transaction = new MissingTransactionResult();
                transaction.setFoundInFile(FoundInFileOption.getInstance(lineArray[0]));
                transaction.setTransactionId(lineArray[1]);
                transaction.setAmount(new BigDecimal(lineArray[2]));
                transaction.setCurrencyCode(lineArray[3]);
                transaction.setValueDate(LocalDate.parse(lineArray[4]));
                resultList.add(transaction);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return resultList;
    }

    private void skipHeader(BufferedReader bufferedReader) throws IOException {
        bufferedReader.readLine();
    }


    private void processInvalidData(BiConsumer<String, TransactionResultType> biConsumer) {
        biConsumer.accept("invalid-transactions-result", new MatchingTransactionResult("TR-47884222201", new BigDecimal("140"), "USD",
                LocalDate.of(2020, 1, 20)));
    }

    private void doMockProcess(BiConsumer<String, TransactionResultType> biConsumer) {
        biConsumer.accept("matching-transactions", new MatchingTransactionResult("TR-47884222201", new BigDecimal("140"), "USD",
                LocalDate.of(2020, 1, 20)));
        biConsumer.accept("matching-transactions", new MatchingTransactionResult("TR-47884222203", new BigDecimal("5000"), "JOD",
                LocalDate.of(2020, 1, 25)));
        biConsumer.accept("matching-transactions", new MatchingTransactionResult("TR-47884222206", new BigDecimal("500"), "USD",
                LocalDate.of(2020, 2, 10)));
        biConsumer.accept("mismatched-transactions", new MisMatchingTransactionResult("TR-47884222202", new BigDecimal("20"), "JOD",
                LocalDate.of(2020, 1, 22), FoundInFileOption.SOURCE));
        biConsumer.accept("mismatched-transactions", new MisMatchingTransactionResult("TR-47884222202", new BigDecimal("30"), "JOD",
                LocalDate.of(2020, 1, 22), FoundInFileOption.TARGET));
        biConsumer.accept("mismatched-transactions", new MisMatchingTransactionResult("TR-47884222205", new BigDecimal("60"), "JOD",
                LocalDate.of(2020, 2, 2), FoundInFileOption.SOURCE));
        biConsumer.accept("mismatched-transactions", new MisMatchingTransactionResult("TR-47884222205", new BigDecimal("60"), "JOD",
                LocalDate.of(2020, 2, 3), FoundInFileOption.TARGET));
        biConsumer.accept("missing-transactions", new MissingTransactionResult("TR-47884222204", new BigDecimal("1200"), "JOD",
                LocalDate.of(2020, 1, 31), FoundInFileOption.SOURCE));
        biConsumer.accept("missing-transactions", new MissingTransactionResult("TR-47884222217", new BigDecimal("12000"), "JOD",
                LocalDate.of(2020, 2, 14), FoundInFileOption.TARGET));
        biConsumer.accept("missing-transactions", new MisMatchingTransactionResult("TR-47884222245", new BigDecimal("420"), "USD",
                LocalDate.of(2020, 1, 12), FoundInFileOption.TARGET));
    }
}
