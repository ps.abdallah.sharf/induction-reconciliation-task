package com.progressoft.jip9;

import com.progressoft.jip9.exception.IllegalTransactionResultException;
import com.progressoft.jip9.exception.PrintResultException;
import com.progressoft.jip9.exception.WhileCreateResultFolderException;
import com.progressoft.jip9.exception.ParsingTransactionException;
import com.progressoft.jip9.resultType.MatchingTransactionResult;
import com.progressoft.jip9.resultType.NonMatchingTransaction;
import com.progressoft.jip9.resultType.TransactionResultType;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.function.BiConsumer;

public class CSVResultPrinter implements ResultPrinter {
    private static final String NON_MATCHING_TRANSACTION_HEADER = "found in file,transaction id,amount,currency code,value date";
    private static final String MATCHING_TRANSACTION_HEADER = "transaction id,amount,currency code,value date";
    private final String destination;

    public CSVResultPrinter(String destinationDirectory) {
        failedIfNull(destinationDirectory, "destination directory");
        this.destination = destinationDirectory + "/reconciliation-results";
    }


    @Override
    public ProcessResult printResult(TransactionProcessor processor, InputStream sourceInputStream, InputStream targetInputStream) {
        failedIfNull(processor, "The processor is null");
        createResultFolder();
        try (FileWriter matchingTransactionWriter = new FileWriter(destination + "/matching-transactions.csv");
             FileWriter misMatchingTransactionWriter = new FileWriter(destination + "/mismatched-transactions.csv");
             FileWriter missingTransactionWriter = new FileWriter(destination + "/missing-transactions.csv")) {
            BiConsumer<String, TransactionResultType> biConsumer = getBiConsumer(matchingTransactionWriter, misMatchingTransactionWriter, missingTransactionWriter);
            prepareFilesHeader(matchingTransactionWriter, misMatchingTransactionWriter, missingTransactionWriter);
            processor.process(sourceInputStream, targetInputStream, biConsumer);
        } catch (ParsingTransactionException | IOException exception) {
            handleException(exception);
        }
        return getProcessResult("Reconciliation finished.");
    }

    private BiConsumer<String, TransactionResultType> getBiConsumer(FileWriter matchingTransactionWriter, FileWriter misMatchingTransactionWriter, FileWriter missingTransactionWriter) {
        return (key, transaction) -> {
            switch (key) {
                case "matching-transactions":
                    writeMatchingEntity(matchingTransactionWriter, (MatchingTransactionResult) transaction);
                    break;
                case "mismatched-transactions":
                    writeNonMatchingEntity(misMatchingTransactionWriter, (NonMatchingTransaction) transaction);
                    break;
                case "missing-transactions":
                    writeNonMatchingEntity(missingTransactionWriter, (NonMatchingTransaction) transaction);
                    break;
                default:
                    throw new IllegalTransactionResultException("Undefined result type: " + key);
            }
        };
    }

    private void handleException(Exception exception) {
        if (exception instanceof ParsingTransactionException) {
            Path path = Paths.get(destination);
            File file = new File(path + "/transaction-reconciliation-error-log-" + LocalDateTime.now());
            try (PrintWriter writer = new PrintWriter(file)) {
                exception.printStackTrace(writer);
                writer.flush();
            } catch (IOException ioException) {
                throw new PrintResultException(ioException);
            }
        }
        throw new PrintResultException(exception);
    }

    private ProcessResult getProcessResult(String result) {
        return new ProcessResult(result, destination);
    }

    private void prepareFilesHeader(FileWriter matchingTransactionWriter, FileWriter misMatchingTransactionWriter, FileWriter missingTransactionWriter) {
        writeLineIntoFile(matchingTransactionWriter, MATCHING_TRANSACTION_HEADER);
        writeLineIntoFile(misMatchingTransactionWriter, NON_MATCHING_TRANSACTION_HEADER);
        writeLineIntoFile(missingTransactionWriter, NON_MATCHING_TRANSACTION_HEADER);
    }


    public void writeMatchingEntity(FileWriter writer, MatchingTransactionResult transaction) {
        StringBuilder line = new StringBuilder();
        extractAndAppendCommonFields(line, transaction);
        writeLineIntoFile(writer, line.toString());
    }

    public void writeNonMatchingEntity(FileWriter writer, NonMatchingTransaction transaction) {
        StringBuilder line = new StringBuilder(transaction.getFoundInFile().name()).append(",");
        extractAndAppendCommonFields(line, transaction);
        writeLineIntoFile(writer, line.toString());

    }

    public void writeLineIntoFile(FileWriter writer, String line) {
        try {
            writer.append(line).append("\n");
            writer.flush();
        } catch (IOException exception) {
            throw new PrintResultException(exception);
        }
    }

    private void extractAndAppendCommonFields(StringBuilder line, TransactionResultType transaction) {
        line.append(transaction.getTransactionId()).append(",")
                .append(transaction.getAmount().toString()).append(",")
                .append(transaction.getCurrencyCode()).append(",")
                .append(transaction.getValueDate().toString());
    }

    private void createResultFolder() {
        try {
            Files.createDirectories(Paths.get(destination));
        } catch (IOException exception) {
            throw new WhileCreateResultFolderException(exception);
        }
    }

    private void failedIfNull(Object object, String name) {
        if (object == null) {
            throw new NullPointerException(name + " is null");
        }
    }
}
