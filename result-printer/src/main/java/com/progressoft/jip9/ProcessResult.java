package com.progressoft.jip9;

public class ProcessResult {

    private final String result;
    private final String resultLocation;

    public ProcessResult(String result, String resultLocation) {
        this.result = result;
        this.resultLocation = resultLocation;
    }

    public String getResult() {
        return result;
    }

    public String getResultLocation() {
        return resultLocation;
    }

}
