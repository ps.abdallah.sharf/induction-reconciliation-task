package com.progressoft.jip9;

import java.io.InputStream;

public interface ResultPrinter {
    ProcessResult printResult(TransactionProcessor processor, InputStream sourcePath, InputStream targetPath);

}
