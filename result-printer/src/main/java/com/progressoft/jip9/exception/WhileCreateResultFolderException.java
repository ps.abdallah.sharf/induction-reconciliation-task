package com.progressoft.jip9.exception;

public class WhileCreateResultFolderException extends RuntimeException {
    public WhileCreateResultFolderException(Exception cause) {
        super(cause);
    }
}
