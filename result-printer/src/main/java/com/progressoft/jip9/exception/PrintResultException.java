package com.progressoft.jip9.exception;

public class PrintResultException extends RuntimeException {
    public PrintResultException(Exception cause) {
        super(cause);
    }
}
