package com.progressoft.jip9.exception;

public class IllegalTransactionResultException extends RuntimeException {
    public IllegalTransactionResultException(String message) {
        super(message);
    }
}
